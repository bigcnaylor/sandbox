// unit_test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <cstdint>
#include <cstdio>
#include <variant>
#include "../core/optional.h"
#include "../variant/variant.h"
#include "../array/fixed.h"
#include "../array/span.h"

struct complicated_int {

   ~complicated_int()
   {
      m_i = 0;
      printf("destroyed complicated int\n");
   }

   complicated_int(int i) :
      m_i(i)
   {
      printf("constructed complicated int\n");
   }

   complicated_int(const complicated_int& o) :
      m_i(o.m_i)
   {
      printf("copy-constructed complicated int\n");
   }

   complicated_int& operator= (const complicated_int& o)
   {
      m_i = o.m_i;
      return *this;
   }

   int m_i;
};

bool operator==(const complicated_int& lhs, const complicated_int& rhs)
{
   return lhs.m_i == rhs.m_i;
}

bool operator!=(const complicated_int& lhs, const complicated_int& rhs)
{
   return lhs.m_i != rhs.m_i;
}

bool operator>(const complicated_int& lhs, const complicated_int& rhs)
{
   return lhs.m_i > rhs.m_i;
}

bool operator<(const complicated_int& lhs, const complicated_int& rhs)
{
   return lhs.m_i < rhs.m_i;
}

struct restricted_int {

   restricted_int(const restricted_int&) = delete;
   restricted_int& operator=(const restricted_int&) = delete;

   int m_i;
};

bool operator==(const restricted_int& lhs, const restricted_int& rhs)
{
   return lhs.m_i == rhs.m_i;
}

bool operator!=(const restricted_int& lhs, const restricted_int& rhs)
{
   return lhs.m_i != rhs.m_i;
}

struct number_printer {

   int operator() (const crn::monostate) const
   {
      return printf("Empty\n");
   }

   int operator() (const int i) const
   {
      return printf("Int = %d\n", i);
   }

   int operator() (const float f) const
   {
      return printf("Float = %0.2f\n", f);
   }

   int operator() (const complicated_int& ci) const
   {
      return printf("Complicated int = %d\n", ci.m_i);
   }
};

int main()
{
   //
   // array

   crn::array::fixed<int, 10> my_first_array;
   printf("array size: %u\n", my_first_array.size());

   const auto my_first_span = crn::array::make_span(my_first_array);
   printf("span size:  %u\n", my_first_span.size());

   //
   // optional

   crn::optional<int> maybe_int = 2;
   crn::compact_optional<int16_t> maybe_other_int = crn::nullopt;
   CRN_ASSERT_NO_MSG(maybe_int > maybe_other_int);

   //swap(maybe_other_int, maybe_int);
   //CRN_ASSERT_NO_MSG(maybe_int < maybe_other_int);

   maybe_int = maybe_other_int;
   CRN_ASSERT_NO_MSG(maybe_int == maybe_other_int);

   static_assert(std::is_trivially_destructible_v<crn::optional<int>>);
   static_assert(std::is_trivially_copy_constructible_v<crn::optional<int>>);
   static_assert(std::is_trivially_move_constructible_v<crn::optional<int>>);
   static_assert(std::is_trivially_copy_assignable_v<crn::optional<int>>);
   static_assert(std::is_trivially_move_assignable_v<crn::optional<int>>);

   static_assert(!std::is_trivially_destructible_v<complicated_int>);
   static_assert(!std::is_trivially_copy_constructible_v<complicated_int>);
   static_assert(!std::is_trivially_move_constructible_v<complicated_int>);
   static_assert(!std::is_trivially_copy_assignable_v<complicated_int>);
   static_assert(!std::is_trivially_move_assignable_v<complicated_int>);

   static_assert(!std::is_trivially_destructible_v<crn::optional<complicated_int>>);
   static_assert(!std::is_trivially_copy_constructible_v<crn::optional<complicated_int>>);
   static_assert(!std::is_trivially_move_constructible_v<crn::optional<complicated_int>>);
   static_assert(!std::is_trivially_copy_assignable_v<crn::optional<complicated_int>>);
   static_assert(!std::is_trivially_move_assignable_v<crn::optional<complicated_int>>);
   static_assert(std::is_destructible_v<crn::optional<complicated_int>>);
   static_assert(std::is_copy_constructible_v<crn::optional<complicated_int>>);
   static_assert(std::is_move_constructible_v<crn::optional<complicated_int>>);
   static_assert(std::is_copy_assignable_v<crn::optional<complicated_int>>);
   static_assert(std::is_move_assignable_v<crn::optional<complicated_int>>);

   static_assert(!std::is_copy_constructible_v<restricted_int>);
   static_assert(!std::is_move_constructible_v<restricted_int>);
   static_assert(!std::is_copy_assignable_v<restricted_int>);
   static_assert(!std::is_move_assignable_v<restricted_int>);

   static_assert(!std::is_copy_constructible_v<crn::optional<restricted_int>>);
   static_assert(!std::is_move_constructible_v<crn::optional<restricted_int>>);
   static_assert(!std::is_copy_assignable_v<crn::optional<restricted_int>>);
   static_assert(!std::is_move_assignable_v<crn::optional<restricted_int>>);

   //
   // variant

   using maybe_number = crn::variant<crn::monostate, int, float, complicated_int, float>;
   maybe_number first_number;
   const maybe_number second_number(crn::variant_index<4>, 0.f);


   if (first_number != second_number) {
      first_number = second_number;
   }

   {
      first_number = complicated_int{1};
   }

   const auto print_result = second_number.visit(number_printer{});
   static_assert(std::is_same<const int, decltype(print_result)>::value, "Wat!");

   {
      using namespace std;
      float const* const p1 = second_number.get_if<4>();
      if (!p1) {
         printf("Wrong alternative.\n");
      } else {
         printf("Here a float: %0.2f\n", *p1);
      }

      float const* const p2 = get_if<4>(second_number);
      if (!p2) {
         printf("Wrong alternative again.\n");
      } else {
         printf("Here a float also: %0.2f\n", *p2);
      }
   }

   {
      using namespace std;

      maybe_number third_number{crn::variant_index<2>, second_number.get<4>()};
      CRN_ASSERT_NO_MSG(third_number < second_number);

      const auto print_1st_third_comp = [&] () {
         if (first_number < third_number) {
            printf("1st number is less than 3rd number.\n");
         } else if (first_number > third_number) {
            printf("1st number is greater than 3rd number.\n");
         } else {
            CRN_ASSERT_NO_MSG(first_number == third_number);
            printf("1st and 3rd numbers are equivalent.\n");
         }
      };

      crn::visit(
         [] (const auto& lhs, auto& rhs) {
            printf("Numbers 1 and 3:\n");

            const auto printer = number_printer{};
            printer(lhs);
            printer(rhs);

            return 0;
         },
         first_number,
         third_number);
      print_1st_third_comp();

      printf("Swap 1 and 3...\n");
      swap(first_number, third_number);

      crn::visit(
         [] (const auto& lhs, auto& rhs) {
            printf("Numbers 1 and 3:\n");

            const auto printer = number_printer{};
            printer(lhs);
            printer(rhs);

            return 0;
         },
         first_number,
         third_number);
      print_1st_third_comp();
   }

   printf("Second number switch:\n");
   switch (second_number.index()) {
   case 0:
      printf("Monostate");
      break;
   case 1:
      printf("\tInt = %d\n", second_number.get_unchecked<1>());
      break;
   case 2:
      printf("\tFloat (2) = %0.2f\n", second_number.get_unchecked<2>());
      break;
   case 3:
      printf("\tComplicated int = %d\n", second_number.get_unchecked<3>().m_i);
      break;
   case 4:
      printf("\tFloat (4) = %0.2f\n", second_number.get_unchecked<4>());
      break;
   default:
      CRN_ASSERT_NO_MSG(false, "Unexpected alternative.");
      break;
   }

   return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
