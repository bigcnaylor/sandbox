#pragma once

/*
#include <cstdint>
#include <memory>
#include <type_traits>
#include "growth_profile.h"
#include "types.h"

namespace crn {
	namespace array {

		// Type trait to determine if a given storage type can be re-allocated
		template <typename Storage, typename = std::void_t<>>
		struct storage_can_reallocate : public std::false_type {};

		template <typename Storage>
		struct storage_can_reallocate < Storage, std::void_t<decltype(std::declval<Storage>().set_allocation({})) >> : public std::true_type {};

		template <typename Storage>
		inline constexpr bool storage_can_reallocate_v = storage_can_reallocate<Storage>::value;

		// Storage with a fixed in-place capacity. Cannot be reallocated
		//
		template <typename Block, size_type Capacity>
		class storage_fixed {
		public:
			using block_type = Block;
			using allocated_blocks = allocation<block_type>;
			static_assert(std::is_trivial_v<block_type>);

			size_type size() const
			{
				return m_size;
			}

			size_type& size_ref() const
			{
				return m_size;
			}

			size_type capacity() const
			{
				return Capacity;
			}

			block_type* data() const
			{
				return m_data;
			}

			allocated_blocks get_allocation() const
			{
				return { m_data, Capacity, allocation_flags::IS_EXTERNAL };
			}

		private:
			block_type m_data[Capacity];
			size_type m_size = 0;
		};

		// Array storage which is allocated as needed from an instance of the spacified allocator
		//
		template <typename Block>
		class storage_dynamic {
		public:
			using block_type       = Block;
			using allocated_blocks = allocation<block_type>;
			static_assert(std::is_trivial_v<block_type>);

			size_type size() const
			{
				return m_size;
			}

			size_type& size_ref() const
			{
				return m_size;
			}

			size_type capacity() const
			{
				return m_blocks.m_capacity;
			}

			block_type* data() const
			{
				return m_blocks.m_p_data;
			}

			allocated_blocks get_allocation() const
			{
				return m_allocated_blocks;
			}

			allocated_blocks set_allocation(const allocated_blocks blocks)
			{
				return m_allocated_blocks = blocks;
			}

		private:
			allocated_blocks m_blocks = { nullptr, 0, 0 };
			size_type m_size = 0;
		};

		// Array storage which initially uses a fixed size buffer, but will grow dynamically if more space is required
		//
		template <typename Block, size_type InplaceCapacity>
		class storage_inplace_dynamic {
		public:
			using block_type       = Block;
			using allocated_blocks = allocation<block_type>;
			static_assert(std::is_trivial_v<block_type>);

			static constexpr size_type inplace_capacity = std::max(InplaceCapacity, sizeof(allocated_blocks) / sizeof(block_type));

			size_type size() const
			{
				return m_size;
			}

			size_type& size_ref() const
			{
				return m_size;
			}

			size_type capacity() const
			{
				return m_is_inplace ? inplace_capacity : m_allocated_blocks.m_capcity;
			}

			block_type* data() const
			{
				return m_is_inplace ? m_inplace_data : m_allocated_blocks.m_p_data;
			}

			allocated_blocks get_allocation() const
			{
				return m_is_inplace
					? allocated_blocks{ m_inplace_data, inplace_capacity, allocation_flags::IS_EXTERNAL }
					: m_allocated_blocks;
			}

			allocated_blocks set_allocation(const allocated_blocks blocks)
			{
				if (blocks.m_p_data != nullptr) {
					m_is_inplace = false;
					return m_allocated_blocks = blocks;
				}

				m_is_inplace = true;
				return { m_inplace_data, inplace_capacity, allocation_flags::IS_EXTERNAL };
			}

		private:

			union {
				allocated_blocks m_allocated_blocks;
				block_type       m_inplace_data[inplace_capacity];
			}
			size_type m_size  = 0;
			bool m_is_inplace = true;
		};

		// Storage which is a type erased wrapper around another storage object
		//
		template <typename Block>
		class storage_proxy {
		public:
			using block_type       = Block;
			using allocated_blocks = allocation<block_type>;
			static_assert(std::is_trivial_v<block_type>);

			template <typename S>
			explicit storage_proxy(S& source) :
				m_p_size(&source.size_ref()),
				m_allocated_blocks(source.get_allocation())
			{
			}

			size_type size() const
			{
				return *m_p_size;
			}

			size_type& size_ref() const
			{
				return *m_p_size;
			}

			size_type capacity() const
			{
				return m_allocated_blocks.m_capacity;
			}

			block_type* data() const
			{
				return m_allocated_blocks.m_p_data;
			}

			allocated_blocks get_allocation() const
			{
				return m_allocated_blocks;
			}

			allocated_blocks set_allocation(const allocated_blocks blocks)
			{
				return m_allocated_blocks = blocks;
			}

		private:
			size_type*       m_p_size;
			allocated_blocks m_allocated_blocks;
		};
	}
}

*/