#pragma once

#include <cstdint>
#include <limits>
#include <type_traits>

namespace crn {
	namespace array {
		using size_type = uint32_t;
		inline constexpr size_type MAX_SIZE = std::numeric_limits<size_type>::max();

		template <typename T>
		using block = std::aligned_storage_t<sizeof(T), alignof(T)>;

		namespace allocation_flags {
			enum enum_type : uint8_t {
				IS_EXTERNAL = 1 << 0,
			};

			using underlying_type = std::underlying_type_t<enum_type>;
		}

		template <typename Block>
		struct allocation {
			Block*    m_p_data;
			size_type m_capacity;
			allocation_flags::underlying_type m_flags;
		};

		enum class fill_types : unsigned int {
			ORDERED,
			UNORDERED,
		};

		// A type tag used to construct dynamic arrays with an initial number of elemetns
		template <size_type N>
		struct in_place_count_t {
			explicit in_place_count_t() = default;
		};

		template <size_type N>
		inline constexpr in_place_count_t<N> in_place_count{};
	}
}
