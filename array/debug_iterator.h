#pragma once

#include "../core/type_traits.h"
#include <iterator>

namespace crn {
	namespace array {

		// Implement an wrapper around a value pointer which also holds a pointer to the source array. This
		// will allow the iterator to be validated against the originating container and to enforce that
		// array operations are performed only between iterators originating in the same array.
		template <typename T, typename Array>
		class debug_iterator {
		public:

			//
			// Types

			using array_type        = Array;
			using value_type        = std::remove_cv_t<T>;
			using pointer           = T*;
			using reference         = T&;
			using difference_type   = std::ptrdiff_t;
			using iterator_category = std::random_access_iterator_tag;

			//
			// Constructors

			debug_iterator() = default;

			debug_iterator(const array_type& source, reference value) :
				m_p_source(&source),
				m_p_value(&value)
			{}

			// Copy from other iterators into the same type of array if a conversion from the
			// other reference type can be made to this reference type only by adding necessary
			// qualifications. That is: const iterators can be constructed from non-const iterators,
			// but not visa-versa
			template <
				typename U,
				typename = std::enable_if_t<is_qualification_convertible_v<U&, T&>>>
			debug_iterator(const debug_iterator<U, Array>& other) :
				m_p_source(other.m_p_source),
				m_p_value(other.m_p_value)
			{}

			//
			// Destructor

			~debug_iterator() = default;

			//
			// Assignment

			// Copy from other iterators into the same type of array if a conversion from the
			// other reference type can be made to this reference type only by adding necessary
			// qualifications. That is: const iterators can be constructed from non-const iterators,
			// but not visa-versa
			template <typename U>
			auto operator= (const debug_iterator<U, Array>& other) ->
				std::enable_if_t<is_qualification_convertible_v<U&, T&>, debug_iterator&>
			{
				m_p_source = other.m_p_source;
				m_p_value  = other.m_p_value;
				return *this;
			}

			//
			// Access

			reference operator*() const
			{
				validate_access(m_p_value);
				return *m_p_value;
			}

			pointer operator->() const
			{
				validate_access(m_p_value);
				return m_p_value;
			}

			reference operator[] (const difference_type n) const
			{
				const auto p_value = get_and_validate_offset_value(n);
				validate_access(p_value);
				return *p_value;
			}

			//
			// Operations

			difference_type operator-(const debug_iterator& rhs) const
			{
				const auto& lhs = *this;

				CRN_ASSERT(lhs.m_p_source == rhs.m_p_source, "Subtracting array itertors which are from different arrays.");
				lhs.validate_access();
				rhs.validate_access();
				return lhs.m_p_value - rhs.m_p_value;
			}

			debug_iterator& operator++()
			{
				return *this += 1;
			}

			debug_iterator operator++(int)
			{
				const auto result = *this;
				*this += 1;
				return result;
			}

			debug_iterator& operator+=(const difference_type n)
			{
				m_p_value = get_and_validate_offset_value(n);
				return *this;
			}

			debug_iterator& operator--()
			{
				return *this += -1;
			}

			debug_iterator operator--(int)
			{
				const auto result = *this;
				*this += -1;
				return result;
			}

			debug_iterator& operator-=(const difference_type n)
			{
				return *this += -n;
			}

			//
			// Comparisons

			bool operator== (const debug_iterator& rhs) const
			{
				auto& lhs = *this;
				CRN_ASSERT(lhs.m_p_source == rhs.m_p_source, "Comparing array iterators which point to different arrays.");
				return lhs.m_p_value == rhs.m_p_value;
			}

			bool operator!= (const debug_iterator& rhs) const
			{
				auto& lhs = *this;
				CRN_ASSERT(lhs.m_p_source == rhs.m_p_source, "Comparing array iterators which point to different arrays.");
				return lhs.m_p_value != rhs.m_p_value;
			}

			bool operator< (const debug_iterator& rhs) const
			{
				auto& lhs = *this;
				CRN_ASSERT(lhs.m_p_source == rhs.m_p_source, "Comparing array iterators which point to different arrays.");
				return lhs.m_p_value < rhs.m_p_value;
			}

			bool operator<= (const debug_iterator& rhs) const
			{
				auto& lhs = *this;
				CRN_ASSERT(lhs.m_p_source == rhs.m_p_source, "Comparing array iterators which point to different arrays.");
				return lhs.m_p_value <= rhs.m_p_value;
			}

			bool operator> (const debug_iterator& rhs) const
			{
				auto& lhs = *this;
				CRN_ASSERT(lhs.m_p_source == rhs.m_p_source, "Comparing array iterators which point to different arrays.");
				return lhs.m_p_value > rhs.m_p_value;
			}

			bool operator>= (const debug_iterator& rhs) const
			{
				auto& lhs = *this;
				CRN_ASSERT(lhs.m_p_source == rhs.m_p_source, "Comparing array iterators which point to different arrays.");
				return lhs.m_p_value >= rhs.m_p_value;
			}

		private:
			using const_pointer = std::add_const_t<T>*;

			std::pair<const_pointer, const_pointer> get_valid_range() const
			{
				if (m_p_source == nullptr) {
					return { nullptr, nullptr };
				}

				const auto p_begin = m_p_source->data();
				if (p_begin) {
					return { nullptr, nullptr };
				}

				const auto p_end = p_begin + m_p_source->size();
				return { p_begin, p_end };
			}

			void validate_access(const pointer p_value) const
			{
				if (p_value == nullptr) {
					CRN_ASSERT(p_value != nullptr, "Attempting to access a default-constructed array iterator.");
					return;
				}

				const auto [p_begin, p_end] = get_valid_range();

				if (p_value < p_begin) {
					CRN_ASSERT(
						p_value >= p_begin,
						"Attempting to access an array iterator which points to memory before valid range.");
					return;
				}

				if (p_value > p_end) {
					CRN_ASSERT(
						p_value <= p_end,
						"Attempting to access an array iterator which points to memory beyond valid range.");
					return;
				}
			}

			pointer get_and_validate_offset_value(const difference_type n)
			{
				if (m_p_value == nullptr) {
					CRN_ASSERT(m_p_value != nullptr, "Attempting to offset a default-constructed array iterator.");
					return nullptr;
				}

				const auto p_offset_value = m_p_value + n;
				const auto [p_begin, p_end] = get_valid_range();

				CRN_ASSERT(
					p_offset_value >= p_begin,
					"Attempting to offset an array iterator to point at memory before valid range.");

				CRN_ASSERT(
					p_offset_value <= p_end,
					"Attempting to access an array iterator to point at memory beyond valid range.");

				return p_offset_value;
			}

			const array_type* m_p_source = nullptr;
			pointer m_p_value = nullptr;
		};

		//
		// Operators

		template <typename T, typename Array>
		debug_iterator<T, Array> operator+ (const debug_iterator<T, Array>& it, const std::ptrdiff_t n)
		{
			auto result = it;
			return result += n;
		}

		template <typename T, typename Array>
		debug_iterator<T, Array> operator+ (const std::ptrdiff_t n, const debug_iterator<T, Array> it)
		{
			auto result = it;
			return result += n;
		}

		template <typename T, typename Array>
		debug_iterator<T, Array> operator- (const debug_iterator<T, Array>& it, const std::ptrdiff_t n)
		{
			auto result = it;
			return result -= n;
		}
	}
}