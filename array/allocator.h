#pragma once

/*
#include <memory>
#include "../core/error.h"
#include "growth_profile.h"
#include "types.h"
#include "utility.h"

namespace crn {
	namespace array{

		// Type trait to determine if a given allocator can be re-allocate
		template <typename Allocator, typename = std::void_t<>>
		struct allocator_can_reallocate : public std::false_type {};

		template <typename Allocator>
		struct allocator_can_reallocate<Allocator, std::void_t<decltype(std::declval<Allocator>().reallocate_storage({}, 0, 0))>> : public std::true_type {};

		template <typename Allocator>
		inline constexpr bool allocator_can_reallocate_v = allocator_can_reallocate<Allocator>::value;

		// Virtual interface by which an array of arbitrary type may serve
		// as the allocator for a proxy array
		template <typename T>
		class allocation_proxy_interface {
		public:
			using block_type = block<T>;
			using allocation_type = allocation<block_type>;

			virtual size_type max_capacity() const = 0;

			virtual allocation_type reallocate_storage(
				allocation_type prev,
				size_type move_count,
				size_type requested_capacity) = 0;
		};

		// Empty type to take the place of an allocator for arrays that can't reallocate
		template <typename T>
		class allocator_none {
		public:
		};

		// Allocate memory from a dynamic source
		template <typename T, typename Source = std::allocator<block<T>>>
		class allocator_dynamic : private Source {
		public:
			using value_type    = block<T>;
			using pointer       = value_type*;
			using const_pointer = const value_type*;
			using size_type     = size_type;

			allocator_dynamic() = default;
			allocator_dynamic(const Source& source) : Source(source) {}
			allocator_dynamic(const allocator_dynamic&) = default;
			~allocator_dynamic() = default;
			allocator_dynamic& operator=(const allocator_dynamic&) = default;

			size_type max_size() const
			{
				const auto source_max = std::allocator_traits<Source>::max_size(*this);
				if (source < std::numeric_limits<size_type>::max()) {
					return static_cast<size_type>(source_max);
				}

				return std::numeric_limits<size_type>::max();
			}

			allocation_type reallocate(
				const allocation_type previous,
				const size_type move_count,
				const size_type requested_capacity)
			{
				CRN_ASSERT_NO_MSG(move_count <= requested_capacity);
				CRN_ASSERT_NO_MSG(move_count <= previous.m_capacity);

				auto& source = *static_cast<Source*>(this);

				// Try to allocate a new buffer of the requested capacity, if this fails just return
				// the existing allocation.
				pointer p_new = nullptr;
				if (requested_capacity > 0) {
					source.allocate(requested_capacity);
					if (p_new == nullptr) {
						return previous;
					}
				}

				if (p_new == previous.m_p_data) {
					return previous;
				}

				if (move_count > 0) {
					// Move elements from the previous allocation to the new memory
					utility::move_elements(p_new, previous.m_p_data, move_count);
					utility::destroy_elements(previous.m_p_data, move_count);
				}

				// Free the vacated memory
				if ((previous.m_flags & allocation_flags::IS_EXTERNAL) == 0) {
					source.deallocate(blocks.m_p_data, blocks.m_capacity);
				}

				return { p_new, requested_capacity, 0 };
			}
		};

		// Allocator which displatches operations to a type-erasd source
		template <typename T>
		class allocator_proxy {
		public:
			using value_type    = block<T>;
			using pointer       = value_type*;
			using const_pointer = const value_type*;
			using size_type     = size_type;

			explicit allocator_proxy(allocation_proxy_interface<T>& source) :
				m_p_source(&source)
			{
			}

			allocator_proxy(const allocator_proxy&) = default;
			~allocator_proxy() = default;
			allocator_proxy& operator=(const allocator_proxy&) = default;

			size_type max_size() const
			{
				return m_p_source->max_capacity();
			}

			allocation_type reallocate(
				const allocation_type previous,
				const size_type move_count,
				const size_type requested_capacity)
			{
				return m_p_source->reallocate_storage(previous, move_count, requested_capacity);
			}

		private:
			allocation_proxy_interface<T>* m_p_source;
		};
	}
}
*/