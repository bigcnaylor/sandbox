#pragma once

#include <algorithm>
#include <array>
#include <numeric>
#include <type_traits>
#include "../core/type_traits.h"
#include "allocator.h"
#include "base.h"
#include "storage.h"
#include "types.h"
#include "utility.h"

namespace crn {
	namespace array {

		//
		// Types

		// An array with variable size (current number of elements), but a fixed
		// capacity (maximum number of elements)
		//
		template <typename T, size_type N>
		class fixed : public base<T, fixed<T,N>> {
		public:
			using base_type = base<T, fixed<T, N>>;
			using typename base_type::value_type;
			using typename base_type::block_type;

			//
			// Construction

			fixed() :
				m_size(0)
			{}

			fixed(const fixed& other) :
				m_size(other.m_size)
			{
				utility::copy_construct_elements(m_data, other.data(), other.m_size);
			}

			fixed(fixed&& other) :
				m_size(other.m_size)
			{
				utility::move_construct_elements(m_data, other.data(), other.m_size);
			}

			template <size_type M, typename = std::enable_if_t<M <= N>>
			fixed(const value_type (&other)[M]) :
				m_size(M)
			{
				utility::copy_construct_elements(m_data, other, M);
			}

			template <size_type M, typename = std::enable_if_t<M <= N>>
			fixed(value_type (&&other)[M]) :
				m_size(M)
			{
				utility::move_construct_elements(m_data, other, M);
			}

			template <size_type M, typename = std::enable_if_t<M <= N>>
			fixed(const std::array<value_type, M>& other) :
				m_size(M)
			{
				utility::copy_construct_elements(m_data, other.data(), M);
			}

			template <size_type M, typename = std::enable_if_t<M <= N>>
			fixed(std::array<value_type, M>&& other) :
				m_size(M)
			{
				utility::move_construct_elements(m_data, other.data(), M);
			}

			template <size_type M, typename = std::enable_if_t<M <= N>>
			fixed(in_place_count_t<M>) :
				m_size(M)
			{
				utility::construct_elements_from_args(m_data, M);
			}

			template <size_type M, typename = std::enable_if_t<M <= N>>
			fixed(in_place_count_t<M>, const value_type& value) :
				m_size(M)
			{
				utility::construct_elements_from_args(m_data, M, value);
			}

			template <typename InputIteratorType, typename = std::enable_if_t<is_legacy_input_iterator_v<InputIteratorType>>>
			fixed(InputIteratorType first, InputIteratorType last)
			{
				const std::size_t range_size = std::distance(first, last);
				CRN_ASSERT(range_size <= N, "Initializer range size %u cannot fit in fixed array with capacity %u.", range_size, N);

				const auto size = range_size < N ? static_cast<size_type>(range_size) : N;

				utility::copy_construct_elements_from_range(m_data, first, last);
				m_size = size;
			}

			fixed(const std::initializer_list<value_type> il)
			{
				const std::size_t range_size = il.size();
				CRN_ASSERT(il.size() <= N, "Initializer list size %u cannot fit in fixed array with capacity %u.", range_size, N);

				const auto size = range_size < N ? static_cast<size_type>(range_size) : N;

				utility::copy_construct_elements(m_data, il.begin(), size);
				m_size = size;
			}

			~fixed() = default;

			//
			// Assignment

			fixed& operator= (const fixed& rhs)
			{
				auto& lhs = *this;

				// Can't swap an array into itself
				if (&lhs != &rhs) {
					utility::copy_array_to_array(lhs.data(), lhs.m_size, rhs.data(), rhs.m_size);
				}

				return lhs;
			}

			fixed& operator= (fixed&& rhs)
			{
				auto& lhs = *this;

				// Can't swap an array into itself
				if (&lhs != &rhs) {
					utility::move_array_to_array(lhs.data(), lhs.m_size, rhs.data(), rhs.m_size);
				}

				return lhs;
			}

			template <size_type M>
			auto operator= (const value_type (&rhs)[M]) -> std::enable_if_t<M <= N, fixed&>
			{
				utility::copy_array_to_array(this->data(), m_size, rhs, M);
				return *this;
			}

			template <size_type M>
			auto operator= (value_type(&&rhs)[M]) -> std::enable_if_t<M <= N, fixed&>
			{
				utility::move_array_to_array(this->data(), m_size, rhs, M);
				return *this;
			}

			template <size_type M>
			auto operator= (const std::array<value_type, M>&rhs) -> std::enable_if_t<M <= N, fixed&>
			{
				utility::copy_array_to_array(this->data(), m_size, rhs.data(), M);
				return *this;
			}

			template <size_type M>
			auto operator= (std::array<value_type, M>&& rhs) -> std::enable_if_t<M <= N, fixed&>
			{
				utility::move_array_to_array(this->data(), m_size, rhs.data(), M);
				return *this;
			}

			fixed& operator= (const std::initializer_list<value_type> il)
			{
				this->assign(il.begin(), il.end());
				return *this;
			}

			//
			// Swap

			void swap(fixed& rhs)
			{
				auto& lhs = *this;

				// Can't swap an array into itself
				if (&lhs == &rhs) {
					return;
				}

				// This fixed-size arrays are the same size so we don't need to worry about any
				// allocation going on (yay), but we also must swap actual element values. This
				// is a 4-step process:
				//
				// 1. Swap values for all indices which exist in both arrays
				// 2. Move-construct elements beyond the range of the smaller array from the larger into the smaller
				// 3. Destroy elements beyond the range of the smaller array from the longer
				// 4. Swap the sizes

				const size_type lhs_size = lhs.m_size;
				const size_type rhs_size = rhs.m_size;

				const auto lhs_data = lhs.data();
				const auto rhs_data = rhs.data();

				if (lhs_size > rhs_size) {
					utility::swap_elements(lhs_data, rhs_data, rhs_size);

					const auto move_begin      = lhs_data + rhs_size;
					const size_type move_count = lhs_size - rhs_size;
					utility::move_construct_elements(rhs_data + rhs_size, move_begin, move_count);
					utility::destroy_elements(move_begin, move_count);
				} else if (rhs_size > lhs_size) {

					utility::swap_elements(lhs_data, rhs_data, lhs_size);

					const auto move_begin      = rhs_data + lhs_size;
					const size_type move_count = rhs_size - lhs_size;
					utility::move_construct_elements(lhs_data + lhs_size, move_begin, move_count);
					utility::destroy_elements(move_begin, move_count);
				} else {

					utility::swap_elements(lhs_data, rhs_data, lhs_size);
				}

				// Swap the sizes
				lhs.m_size = rhs_size;
				rhs.m_size = lhs_size;
			}

		private:

			//
			// Base class access to data

			friend class base<T, fixed<T, N>>;

			block_type* get_data()
			{
				return m_data;
			}
			
			const block_type* get_data() const
			{
				return m_data;
			}

			size_type get_capacity() const
			{
				return N;
			}

			size_type get_size() const
			{
				return m_size;
			}

			size_type& get_size_ref()
			{
				return m_size;
			}

			//
			// Data

			block_type m_data[N];
			size_type m_size;
		};

		//
		// Free functions

		// Specialize the swap function for fixed-size arrays
		template <typename T, size_type N>
		void swap(fixed<T, N>& lhs, fixed<T, N>& rhs)
		{
			lhs.swap(rhs);
		}
	}
}
