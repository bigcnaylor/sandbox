#pragma once

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <iterator>
#include <numeric>
#include <optional>
#include <type_traits>
#include <utility>
#include "../core/casts.h"
#include "../core/error.h"
#include "../core/type_traits.h"
#include "debug_iterator.h"
#include "growth_profile.h"
#include "types.h"
#include "utility.h"

#if defined(_DEBUG)
	#define CRN_ARRAY_USE_DEBUG_ITERATOR() 1
#else
	#define CRN_ARRAY_USE_DEBUG_ITERATOR() 0
#endif

namespace crn {
	namespace array {

		//
		// Types

		// Type traits for array implementations
		//
		namespace implementation_spec {
			// Data (raw memory) - get_data()
			template <typename I, typename = std::void_t<>>
			struct provides_get_data : std::false_type {};

			template <typename I>
			struct provides_get_data<I, std::void_t<decltype(std::declval<I>().get_data())>> : std::true_type {};

			// Capacity - get_capacity()
			template <typename I, typename = std::void_t<>>
			struct provides_get_capacity : std::false_type {};

			template <typename I>
			struct provides_get_capacity<I, std::enable_if_t<std::is_same_v<decltype(std::declval<const I>().get_capacity()), size_type>>> : std::true_type {};

			// Size (value) - get_size()
			template <typename I, typename = std::void_t<>>
			struct provides_get_size : std::false_type {};

			template <typename I>
			struct provides_get_size<I, std::enable_if_t<std::is_convertible_v<decltype(std::declval<const I>().get_size()), size_type>>> : std::true_type {};

			// Size (reference) - get_size_ref()
			template <typename I, typename = std::void_t<>>
			struct provides_get_size_ref : std::false_type {};

			template <typename I>
			struct provides_get_size_ref<I, std::enable_if_t<std::is_convertible_v<decltype(std::declval<I>().get_size_ref()), size_type&>>> : std::true_type {};

			// Reallocations (optional) - reallocate(size)
			template <typename I, typename = std::void_t<>>
			struct provides_reallocate : std::false_type {};

			template <typename I>
			struct provides_reallocate<I, std::void_t<decltype(std::declval<I>().reallocate(0))>> : std::true_type {};

			// Max size - max_size()
			template <typename I, typename = std::void_t<>>
			struct provides_get_max_size : std::false_type {};

			template <typename I>
			struct provides_get_max_size<I, std::enable_if_t<std::is_same_v<decltype(std::declval<const I>().get_max_size()), size_type>>> : std::true_type {};
		}

		// Implement the common array type interface for a specific implementation, which actually has access to the array data
		//
		template <typename T, typename Implementation>
		class base {
		public:
			using value_type             = T;
			using block_type             = block<T>;
			using size_type              = size_type;
			using different_type         = std::ptrdiff_t;
			using reference              = value_type&;
			using const_reference        = const value_type&;
			using pointer                = value_type*;
			using const_pointer          = const value_type*;
#if CRN_ARRAY_USE_DEBUG_ITERATOR()
			using iterator               = debug_iterator<value_type, Implementation>;
			using const_iterator         = debug_iterator<const value_type, Implementation>;
#else
			using iterator               = pointer;
			using const_iterator         = const_pointer;
#endif
			using reverse_iterator       = std::reverse_iterator<iterator>;
			using const_reverse_iterator = std::reverse_iterator<const_iterator>;

			static_assert(std::is_trivial_v<block_type>);
			static_assert(sizeof(block_type)  == sizeof(value_type));
			static_assert(alignof(block_type) == alignof(value_type));

			//
			// Element Access

			reference operator[] (const size_type i)
			{
				CRN_ASSERT_NO_MSG(i < size());
				return data()[i];
			}

			const_reference operator[] (const size_type i) const
			{
				CRN_ASSERT_NO_MSG(i < size());
				return data()[i];
			}

			reference at(const size_type i)
			{
				const size_type size = size();
				if (i >= size) {
					CRN_FATAL_ERROR("Index (%u) out of range (%u).", i, size);
				}

				return data()[i];
			}

			const_reference at(const size_type i) const
			{
				const size_type size = size();
				if (i >= size) {
					CRN_FATAL_ERROR("Index (%u) out of range (%u).", i, size);
				}

				return data()[i];
			}

			reference front()
			{
				CRN_ASSERT_NO_MSG(size() > 0);
				return data()[0];
			}

			const_reference front() const
			{
				CRN_ASSERT_NO_MSG(size() > 0);
				return data()[0];
			}

			reference back()
			{
				const size_type size = size();
				CRN_ASSERT_NO_MSG(size > 0);
				return data()[size - 1];
			}

			const_reference back() const
			{
				const size_type size = size();
				CRN_ASSERT_NO_MSG(size > 0);
				return data()[size - 1];
			}

			pointer data()
			{
				block_type* const p_blocks = as_implementation().get_data();
				return reinterpret_cast<value_type*>(p_blocks);
			}

			const_pointer data() const
			{
				const block_type* const p_blocks = as_implementation().get_data();
				return reinterpret_cast<const value_type*>(p_blocks);
			}

			//
			// Iterators

			iterator begin()
			{
				return make_iterator(0);
			}

			const_iterator begin() const
			{
				return make_const_iterator(0);
			}

			const_iterator cbegin() const
			{
				return make_const_iterator(0);
			}

			iterator end()
			{
				return make_iterator(size());
			}

			const_iterator end() const
			{
				return make_const_iterator(size());
			}

			const_iterator cend() const
			{
				return make_const_iterator(size());
			}

			reverse_iterator rbegin()
			{
				return std::make_reverse_iterator(begin());
			}

			const_reverse_iterator rbegin() const
			{
				return std::make_reverse_iterator(begin());
			}

			const_reverse_iterator crbegin() const
			{
				return std::make_reverse_iterator(begin());
			}

			reverse_iterator rend()
			{
				return std::make_reverse_iterator(end());
			}

			const_reverse_iterator rend() const
			{
				return std::make_reverse_iterator(end());
			}

			const_reverse_iterator crend() const
			{
				return std::make_reverse_iterator(end());
			}

			//
			// Capacity

			// True if there are no elements in the array
			bool empty() const
			{
				return as_implementation().get_size() == 0;
			}

			// The number of elements in the array
			size_type size() const
			{
				return as_implementation().get_size();
			}

			// The number of elements which the array can hold without allocating more memory
			size_type capacity() const
			{
				return as_implementation().get_capacity();
			}

			//
			// Modifiers

			void clear()
			{
				truncate_elements(size());
			}

			void assign(const size_type n, const_reference v)
			{
				const bool success = try_assign(n, v);
				CRN_ASSERT(success, "Failed to assign array to %u copies of the same value.", n);
			}

			template <typename InputIteratorType>
			auto assign(const InputIteratorType first, const InputIteratorType last) ->
				std::enable_if_t<is_legacy_input_iterator_v<InputIteratorType>, void>
			{
				const bool success = try_assign(first, last);
				CRN_ASSERT(success, "Failed to assign array to specified range.");
			}

			void assign(const std::initializer_list<value_type> il)
			{
				const bool success = try_assign(il);
				CRN_ASSERT(success, "Failed to assign array to initializer list.");
			}

			bool try_assign(const size_type n, const_reference v)
			{
				clear();
				return try_append(n, v);
			}

			template <typename InputIteratorType>
			auto try_assign(const InputIteratorType first, const InputIteratorType last) ->
				std::enable_if_t<is_legacy_input_iterator_v<InputIteratorType>, bool>
			{
				clear();
				return try_append(first, last);
			}

			bool try_assign(const std::initializer_list<value_type> il)
			{
				clear();
				return try_append(il);
			}

			void resize(const size_type n)
			{
				resize_along_profile(growth_profile_types::EXACT, n);
			}

			void resize(const size_type n, const value_type& v)
			{
				resize_along_profile(growth_profile_types::EXACT, n, v);
			}

			void resize_along_profile(const growth_profile_types profile, const size_type n)
			{
				const bool success = try_resize_along_profile(profile, n);
				CRN_ASSERT(success, "Failed to resize array to %u elements.", n);
			}

			void resize_along_profile(const growth_profile_types profile, const size_type n, const value_type& v)
			{
				const bool success = try_resize_along_profile(profile, n, v);
				CRN_ASSERT(success, "Failed to resize array to %u elements.", n);
			}

			bool try_resize(const size_type n)
			{
				return try_resize_along_profile(growth_profile_types::EXACT, n);
			}

			bool try_resize(const size_type n, const value_type& v)
			{
				return try_resize_along_profile(growth_profile_types::EXACT, n, v);
			}

			bool try_resize_along_profile(const growth_profile_types profile, const size_type n)
			{
				return try_resize_impl(
					profile,
					n,
					[] (block_type* p_first, block_type* const p_last) {
						for (; p_first != p_last; ++p_first) {
							new (p_first) value_type();
						}
					});
			}

			bool try_resize_along_profile(const growth_profile_types profile, const size_type n, const value_type& v)
			{
				return try_resize_impl(
					profile,
					n,
					[&v] (block_type* p_first, block_type* const p_last) {
						for (; p_first != p_last; ++p_first) {
							new (p_first) value_type(v);
						}
					});
			}

			iterator insert(const const_iterator it, const value_type& v)
			{
				const auto maybe_result = try_insert(it, v);
				CRN_ASSERT(maybe_result, "Array was unable to insert a new element.");
				return *maybe_result;
			}

			iterator insert(const const_iterator it, value_type&& v)
			{
				const auto maybe_result = try_insert(it, std::move(v));
				CRN_ASSERT(maybe_result, "Array was unable to insert a new element.");
				return *maybe_result;
			}

			iterator insert(const const_iterator it, const size_type n, const value_type& v)
			{
				const auto maybe_result = try_insert(it, n, v);
				CRN_ASSERT(maybe_result, "Array was unable to insert a new element.");
				return *maybe_result;
			}

			template <typename InputIteratorType>
			auto insert(const const_iterator it, const InputIteratorType first, const InputIteratorType last) ->
				std::enable_if_t<is_legacy_input_iterator_v<InputIteratorType>, iterator>
			{
				const auto maybe_result = try_insert(it, first, last);
				CRN_ASSERT(maybe_result, "Array was unable to insert a new element.");
				return *maybe_result;
			}

			iterator insert(const const_iterator it, const std::initializer_list<value_type> il)
			{
				const auto maybe_result = try_insert(it, il);
				CRN_ASSERT(maybe_result, "Array was unable to insert a new element.");
				return *maybe_result;
			}

			std::optional<iterator> try_insert(const const_iterator it, const value_type& v)
			{
				return try_emplace(it, v);
			}

			std::optional<iterator> try_insert(const const_iterator it, value_type&& v)
			{
				return try_emplace(it, std::move(v));
			}

			std::optional<iterator> try_insert(const const_iterator it, const size_type n, const value_type& v)
			{
				return try_insert_impl(
					it,
					n,
					[&](block_type* const first_block, block_type* const last_block) {
						CRN_ASSERT_NO_MSG((last_block - first_block) == n);
						utility::construct_elements_from_args<value_type>(first_block, n, v);
					});
			}

			template <typename InputIteratorType>
			auto try_insert(const const_iterator it, const InputIteratorType first, const InputIteratorType last) ->
				std::enable_if_t<crn::is_legacy_input_iterator_v<InputIteratorType>, std::optional<iterator>>
			{
				const auto insert_count = narrow_cast<size_type>(std::distance(first, last));

				return try_insert_impl(
					it,
					insert_count,
					[&](block_type* first_block, block_type* const last_block) {
						CRN_ASSERT_NO_MSG((last_block - first_block) == insert_count);
						for (auto it = first; first_block != last_block;  ++first_block, ++it) {
							new (first_block) value_type(*it);
						}
					});
			}

			std::optional<iterator> try_insert(const const_iterator position, const std::initializer_list<value_type> il)
			{
				return try_insert(position, il.begin(), il.end());
			}

			template <typename... Args>
			iterator emplace(const const_iterator it, Args&&... args)
			{
				const auto maybe_result = try_emplace(it, std::forward<Args>(args)...);
				CRN_ASSERT(maybe_result, "Failed to emplace element into array.");
				return *maybe_result;
			}

			template <typename... Args>
			std::optional<iterator> try_emplace(const const_iterator it, Args&&... args)
			{
				return try_insert_impl(
					it,
					1,
					[&] (block_type* const first_block, block_type* const last_block) {
						CRN_ASSERT_NO_MSG((last_block - first_block) == 1);
						new (first_block) value_type(std::forward<Args>(args)...);
					});
			}

			void push_back(const value_type& v)
			{
				const bool was_pushed = try_push_back(v);
				CRN_ASSERT(was_pushed, "Failed to push new value onto array.");
			}

			void push_back(value_type&& v)
			{
				const bool was_pushed = try_push_back(std::move(v));
				CRN_ASSERT(was_pushed, "Failed to push new value onto array.");
			}

			bool try_push_back(const value_type& v)
			{
				return try_emplace_back(v) != nullptr;
			}

			bool try_push_back(value_type&& v)
			{
				return try_emplace_back(std::move(v)) != nullptr;
			}

			template <typename... Args>
			reference emplace_back(Args&&... args)
			{
				const auto p_emplaced = try_emplace_back(std::forward<Args>(args)...);
				CRN_ASSERT(p_emplaced != nullptr, "Failed to allocate memory for new array element.");
				return *p_emplaced;
			}

			template <typename... Args>
			pointer try_emplace_back(Args&&... args)
			{
				const auto append_result = try_append_impl(
					growth_profile_types::DEFAULT,
					1,
					[&](block_type* first, block_type* const last) {
						CRN_ASSERT_NO_MSG((last - first) == 1);
						utility::construct_elements_from_args(first, 1, std::forward<Args>(args)...);
					});

				if (!append_result) {
					return nullptr;
				}

				const auto append_it = *append_result;
				auto& append_elem = *append_it;
				return &append_elem;
			}

			iterator append(const size_type n)
			{
				const auto append_result = try_append(n);
				CRN_ASSERT(append_result, "Failed to append %u new values to array.", n);
				return append_result;
			}

			iterator append(const size_type n, const value_type& v)
			{
				const auto append_result = try_append(n, v);
				CRN_ASSERT(append_result, "Failed to append %u new values to array.", n);
				return append_result;
			}

			template <typename InputIteratorType>
			auto append(const InputIteratorType first, const InputIteratorType last) ->
				std::enable_if_t<crn::is_legacy_input_iterator_v<InputIteratorType>, iterator>
			{
				const auto try_result = try_append(first, last);
				CRN_ASSERT(try_result, "Failed to append range to array.");
				return *try_result;
			}

			iterator append(const std::initializer_list<value_type> il)
			{
				const auto try_result = try_append(il);
				CRN_ASSERT(try_result, "Failed to append initializer list to array.");
				return *try_result;
			}

			std::optional<iterator> try_append(const size_type n)
			{
				return try_append_impl(
					growth_profile_types::DEFAULT,
					n,
					[&](block_type* const first, block_type* const last) {
						CRN_ASSERT_NO_MSG((last - first) == n);
						utility::construct_elements_from_args(first, n);
					});
			}

			std::optional<iterator> try_append(const size_type n, const value_type& v)
			{
				return try_append_impl(
					growth_profile_types::DEFAULT,
					n,
					[&](block_type* const first, block_type* const last) {
						CRN_ASSERT_NO_MSG((last - first) == n);
						utility::construct_elements_from_args(first, n, v);
					});
			}

			template <typename InputIteratorType>
			auto try_append(const InputIteratorType first, const InputIteratorType last) ->
				std::enable_if_t<crn::is_legacy_input_iterator_v<InputIteratorType>, std::optional<iterator>>
			{
				const auto append_count = narrow_cast<size_type>(std::distance(first, last));

				return try_append_impl(
					growth_profile_types::DEFAULT,
					append_count,
					[&](block_type* first_block, block_type* const last_block) {
						CRN_ASSERT_NO_MSG((last_block - first_block) == append_count);
						for (auto it = first; first_block != last_block; ++first_block, ++it) {
							new (first_block) value_type(*it);
						}
					});
			}

			std::optional<iterator> try_append(const std::initializer_list<value_type> il)
			{
				return try_append(il.begin(), il.end());
			}

			void pop_back()
			{
				truncate_elements(1);
			}

			void truncate(const size_type n)
			{
				truncate_elements(n);
			}

			iterator erase(const const_iterator it)
			{
				// the erase method uses ordered erasure for consistency with the standard library,
				// but in many situations the unordered erase should be prefered
				return erase(fill_types::ORDERED, it);
			}

			iterator erase(const const_iterator first, const const_iterator last)
			{
				// the erase method uses ordered erasure for consistency with the standard library,
				// but in many situations the unordered erase should be prefered
				return erase(fill_types::ORDERED, first, last);
			}

			iterator erase(const fill_types fill_type, const const_iterator it)
			{
				return erase_elements(fill_types, it, 1);
			}

			iterator erase(const fill_types fill_type, const const_iterator first, const const_iterator last)
			{
				const auto count = narrow_cast<size_type>(last - first);
				return erase_elements(fill_types, first, count);

			}

		protected:

			//
			// Construction

			// Protect the constructor and destructor so that they can only be called by derived types
			base() = default;
			~base() = default;

			//
			// Implementation

			// Get a reference to this array as the specific implementation type
			Implementation& as_implementation()
			{
				return *static_cast<Implementation*>(this);
			}

			const Implementation& as_implementation() const
			{
				return *static_cast<const Implementation*>(this);
			}

			//
			// Iterators

			// Create an interator to the specified index in this array 
#if CRN_ARRAY_USE_DEBUG_ITERATOR()
			iterator make_iterator(const size_type index)
			{
				auto& impl = as_implementation();
				CRN_ASSERT_NO_MSG(index <= impl.get_size());

				const auto p_data = reinterpret_cast<value_type*>(impl.get_data());

				return { impl, p_data + index };
			}

			const_iterator make_const_iterator(const size_type index) const
			{
				auto& impl = as_implementation();
				CRN_ASSERT_NO_MSG(index <= impl.get_size());

				const auto p_data = reinterpret_cast<const value_type*>(impl.get_data());

				return { impl, p_data + index };
			}
#else
			iterator make_iterator(const size_type index)
			{
				auto& impl = as_implementation();
				CRN_ASSERT_NO_MSG(index <= impl.get_size());

				const auto p_data = reinterpret_cast<value_type*>(impl.get_data());

				return p_data + index;
			}

			const_iterator make_const_iterator(const size_type index)
			{
				auto& impl = as_implementation();
				CRN_ASSERT_NO_MSG(index <= impl.get_size());

				const auto p_data = reinterpret_cast<const value_type*>(impl.get_data());

				return p_data + index;
			}
#endif

			//
			// Reallocation
			//
			// These functions are used to reallocate the array data, which not all array types support. Since
			// we've already gone down the CRTP hole in order to allow data layout to be speciallized let's keep
			// doing that, though this part could probably be made virtual with minimal fuss since reallocation
			// should be a rare operation.

			template <typename I>
			static void try_reallocate_impl(I& impl, const growth_profile_types profile, const size_type requested_capacity)
			{
				if constexpr (implementation_spec::provides_reallocate<I>::value) {
					const size_type new_capacity = utility::compute_new_capacity(profile, requested_capacity);
					impl.reallocate(new_capacity);
				}
			}

			void try_reallocate(const growth_profile_types profile, const size_type requested_capacity)
			{
				try_reallocate_impl(as_implementation(), profile, requested_capacity);
			}

			//
			// Modifiers

			// Resize the array constructing any new elements required from the provided arguments
			template <typename Constructor>
			bool try_resize_impl(
				const growth_profile_types profile,
				const size_type new_size,
				Constructor&& constructor)
			{
				const size_type init_size = size();
				if (init_size == new_size) {
					return true;
				} else if (new_size < init_size) {
					const size_type pop_count = init_size - new_size;
					truncate_elements(pop_count);
					return true;
				} else {
					const size_type push_count = new_size - init_size;
					return try_append_impl(profile, push_count, std::forward<Constructor>(constructor));
				}
			}

			// Append empty blocks to the end of the storage
			//
			// returns: a pointer to the first pushed block or nullptr on failure
			//
			template <typename Constructor>
			std::optional<iterator> try_append_impl(
				const growth_profile_types profile,
				const size_type n,
				Constructor&& constructor)
			{
				auto& impl = as_implementation();

				size_type& size = impl.get_size_ref();

				const size_type old_size = size;
				const size_type new_size = old_size + n;

				if (new_size > impl.get_capacity()) {
					try_reallocate_impl(impl, profile, new_size);
				}

				if (impl.get_capacity() < new_size) {
					return std::nullopt;
				}

				block_type* const p_data = impl.get_data();
				constructor(p_data + old_size, p_data + new_size);

				size = new_size;
				return make_iterator(old_size);
			}

			// Insert some empty blocks in the middle of storage. Order is preserved
			template <typename Constructor>
			std::optional<iterator> try_insert_impl(
				const const_iterator it,
				const size_type n,
				Constructor&& constructor)
			{
				const auto i_first = narrow_cast<size_type>(it - cbegin());

				// Nothing to insert, return a mutable iterator to the provided position
				if (n == 0) {
					return make_iterator(i_first);
				}

				auto& impl = as_implementation();

				size_type& size = impl.get_size_ref();
				const size_type old_size = size;
				CRN_ASSERT_NO_MSG(i_first <= old_size);

				// Make sure there is room for the inserted elements
				const size_type new_size = old_size + n;

				if (new_size > impl.get_capacity()) {
					try_reallocate_impl(impl, growth_profile_types::DEFAULT, new_size);
				}

				if (impl.get_capacity() < new_size) {
					return std::nullopt;
				}

				// Move values out of the way of the inserted range.
				//
				// Cannot use utility::move_and_destroy_elements(...) because these ranges
				// are expected to overlap, and moving goes in reverse

				block_type* const p_data = impl.get_data();

				const auto p_first_insert  = p_data + i_first;
				const auto p_last_insert   = p_first_insert + n;
				
				const auto p_first_moved = reinterpret_cast<value_type*>(p_first_insert);

				for (auto i = old_size - i_first - 1; i >= 0; --i) {
					value_type& src_value = p_first_moved[i];
					new (p_last_insert + i) value_type(std::move(src_value));
				}

				utility::destroy_elements(p_first_moved, n);

				constructor(p_first_insert, p_last_insert);

				// Return a mutable iterator to the slot where the first element was inserted
				return make_iterator(i_first);
			}

			// Remove the specified number of blocks from the end of storage
			void truncate_elements(const size_type n)
			{
				auto& impl = as_implementation();

				size_type& size = impl.get_size_ref();
				CRN_ASSERT_NO_MSG(n <= size);

				const size_type old_size = size;
				const size_type new_size = old_size - n;

				const auto p_values = reinterpret_cast<value_type*>(impl.get_data());
				utility::destroy_elements(p_values + new_size, n);

				size = new_size;
			}

			// Remove the specified range of blocks from storage. Order is preserved.
			iterator erase_elements(const fill_types fill_type, const const_iterator first, const size_type erase_count)
			{
				const auto array_begin = cbegin();
				const auto first_index = narrow_cast<size_type>(first - array_begin);

				const auto result = make_iterator(first_index);

				if (erase_count == 0) {
					return result;
				}

				// This operation can be separated into 2 steps:
				//  1. Move all the blocks from beyond the erased region back to the range starting at the removed block
				//  2. Pop n blocks off the end

				auto& impl = as_implementation();
				const auto array_size  = impl.get_size();
				const auto array_data  = reinterpret_cast<value_type*>(impl.get_data());

				const auto last_index  = first_index + erase_count;
				CRN_ASSERT_NO_MSG(last_index <= array_size);

				// Move elements to fill the vacated entries
				const auto move_insert = array_data + first_index;
				const auto move_end    = array_data + array_size;

				const auto move_index = [&]() -> size_type {
					switch (fill_type) {
					case fill_types::ORDERED:
						return last_index;

					case fill_types::UNORDERED:
						{
							const auto new_size = narrow_cast<size_type>(array_size - erase_count);
							return std::max(last_index, new_size);
						}
					}
				} ();

				const auto move_begin = array_data + move_index;
				std::move(move_begin, move_end, move_insert);

				// Pop blocks off the end
				truncate_elements(erase_count);

				return result;
			}
		};

		// Implement the common reallocatable array type interface for a specific implementation, which
		// actually has access to the array data
		//
		template <typename T, typename Implementation>
		class reallocatable_base : public base<T, Implementation> {
		public:

			//
			// Capacity

			size_type max_size() const
			{
				return this->as_implementation().get_max_size();
			}

			// Increase the capacity to be greater than or equal to the specified value
			//
			// requested_capacity: The minimum required capacity
			// profile:            How to determine the actual capacity to allocate
			//
			//
			void reserve(const size_type requested_capacity)
			{
				// The C++ standard suggests that calling reserve messes with amortized constant-time
				// growth, so that propably means it doesn't follow the normal growth profile. Using
				// the exact specified should also be more efficient when this is used properly
				reserve_along_profile(growth_profile_types::EXACT, requested_capacity);
			}

			void reserve_along_profile(const growth_profile_types profile, const size_type requested_capacity)
			{
				if (requested_capacity > this->capacity()) {
					this->try_reallocate(profile, requested_capacity);
					CRN_ASSERT(this->capacity() >= requested_capacity, "Unable to reserve requested capacity.");
				}
			}

			// Request the removal of unused capacity
			void shrink_to_fit()
			{
				shrink_to_fit_along_profile(growth_profile_types::EXACT, this->size());
			}

			void shrink_to_fit_along_profile(const growth_profile_types profile)
			{
				// Attempt to allocate a smaller buffer. Shrinking is optional, so don't check for failure (in
				// which case the array should be unmodified).
				this->try_reallocate(profile, this->size());
			}

			//
			// Modifiers

			void clear_and_deallocate()
			{
				this->clear();
				this->try_reallocate(growth_profile_types::EXACT, 0);
			}

		protected:

			//
			// Construction

			reallocatable_base() = default;
			~reallocatable_base() = default;
		};

		//
		// Comparisons

		template <typename T, typename I1, typename I2, typename C>
		bool lexicographical_compare(const base<T, I1>& lhs, const base<T, I2>& rhs, C&& compare)
		{
			const auto lhs_data = lhs.data();
			const auto rhs_data = rhs.data();
			return std::lexicographical_compare(
				lhs_data,
				lhs_data + lhs.size(),
				rhs_data,
				rhs_data + rhs.size(),
				std::forward<C>(compare));
		}

		template <typename T, typename I1, typename I2>
		bool operator== (const base<T, I1>& lhs, const base<T, I2>& rhs)
		{
			const size_type size = lhs.size();
			if (rhs.size() != size) {
				return false;
			}

			const auto lhs_data = lhs.data();
			return std::equal(lhs_data, lhs_data + size, rhs.data());
		}

		template <typename T, typename I1, typename I2>
		bool operator!= (const base<T, I1>& lhs, const base<T, I2>& rhs)
		{
			return !(lhs == rhs);
		}

		template <typename T, typename I1, typename I2>
		bool operator< (const base<T, I1>& lhs, const base<T, I2>& rhs)
		{
			return lexicographical_compare(lhs, rhs, std::less<T>{});
		}

		template <typename T, typename I1, typename I2>
		bool operator<= (const base<T, I1>& lhs, const base<T, I2>& rhs)
		{
			return lexicographical_compare(lhs, rhs, std::less_equal<T>{});
		}

		template <typename T, typename I1, typename I2>
		bool operator> (const base<T, I1>& lhs, const base<T, I2>& rhs)
		{
			return lexicographical_compare(lhs, rhs, std::greater<T>{});
		}

		template <typename T, typename I1, typename I2>
		bool operator>= (const base<T, I1>& lhs, const base<T, I2>& rhs)
		{
			return lexicographical_compare(lhs, rhs, std::greater_equal<T>{});
		}
	}
}