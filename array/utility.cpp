#include "utility.h"

// Compute the number of blocks to allocate according to the specified growth profile
auto crn::array::utility::compute_new_capacity(
	const growth_profile_types profile,
	const size_type requested) -> size_type
{
	using namespace crn::array;

	switch (profile) {
	case growth_profile_types::EXACT:
		return requested;

	case growth_profile_types::DEFAULT:
		return growth_profile_default::compute(requested);
	}

	return requested;
}