#pragma once

#include <memory>
#include "base.h"

namespace crn {
	namespace array {

		//
		// Types

		// An array backed by dynamically-allocated memory
		//
		template <typename T, typename Allocator = std::allocator<block<T>>>
		class dynamic :
			public reallocatable_base<T, dynamic<T, Allocator>>,
			private Allocator {
		public:
			using base_type      = base<T, dynamic<T, Allocator>>;
			using value_type     = typename base_type::value_type;
			using block_type     = typename base_type::block_type;
			using allocator_type = Allocator;

			//
			// Construction

			~dynamic()
			{
				// Destroy any remaining elements and free allocated data on destruction
				if (m_p_data != nullptr) {
					utility::destroy_elements(this->data(), m_size);
					static_cast<allocator_type>(this)->deallocate(m_p_data, m_capacity);
				}
			}

			//
			// Allocator

			allocator_type get_allocator() const
			{
				return *this;
			}

			//
			// Swap

			void swap(dynamic& rhs)
			{
				using std::swap;

				auto& lhs = *this;

				allocator_type& lhs_alloc = lhs;
				allocator_type& rhs_alloc = rhs;
				swap(lhs_alloc, rhs_alloc);

				swap(lhs.m_p_data,   rhs.m_p_data);
				swap(lhs.m_capacity, rhs.m_capacity);
				swap(lhs.m_size,     rhs.m_size);
			}

		private:

			//
			// Base class access to data

			friend class base_type;
			friend class reallocatable_base<T, dynamic<T, Allocator>>;

			block_type* get_data()
			{
				return m_p_data;
			}

			const block_type* get_data() const
			{
				return m_p_data;
			}

			size_type get_capacity() const
			{
				return m_capacity;
			}

			size_type get_size() const
			{
				return m_size;
			}

			size_type& get_size_ref()
			{
				return m_size;
			}

			size_type get_max_size() const
			{
				const std::size_t alloc_max_size = std::allocator_traits<allocator_type>::max_size(*this);
				return alloc_max_size < std::numeric_limits<size_type>::max()
					? static_cast<size_type>(alloc_max_size)
					: std::numeric_limits<size_type>::max();
			}

			void reallocate(const size_type new_capacity)
			{
				allocator_type& allocator = *this;

				// Allcoate new memory to place the elements in
				const block_type p_new = new_capacity != 0 ? allocator.allocate(new_capacity) : nullptr;
				if (p_new == nullptr && new_capacity > 0) {
					// Fail, actually up to the caller whether this is a problem
					return;
				}

				// Construct new elements in the new memory by moving the old elements (if any)
				value_type* const p_old  = this->data();
				const size_type old_size = m_size;

				const size_type new_size = std::min(new_capacity, old_size);
				utility::move_construct_elements(p_new, p_old, new_size);

				// Destroy any old elements
				utility::destroy_elements(p_old, old_size);

				// Free the old memory
				allocator.deallocate(p_old, m_capacity);

				// Update state
				m_p_data   = p_new;
				m_capacity = new_capacity;
				m_size     = new_size;
			}

			//
			// Data

			block_type m_p_data   = nullptr;
			size_type  m_capacity = 0;
			size_type  m_size     = 0;
		};

		//
		// Free functions

		// Specialize the swap function for fixed-size arrays
		template <typename T, typename Allocator>
		void swap(dynamic<T, Allocator>& lhs, dynamic<T, Allocator>& rhs)
		{
			lhs.swap(rhs);
		}
	}
}
