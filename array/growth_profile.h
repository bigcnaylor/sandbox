#pragma once

#include <cmath>
#include <numeric>
#include "../core/error.h"
#include "types.h"

namespace crn {
	namespace array {

		// A growth profile which defines raw exponential growth of a factor defined by a rational number
		template <unsigned int FactorNum, unsigned int FactorDenom>
		struct growth_profile_exponential {
			static_assert(FactorDenom > 0, "division by zero");
			static_assert(FactorDenom < FactorNum, "not actually growth");

			// Use logorithms to actually compute an actual power of the configured factor
			static constexpr float FACTOR = static_cast<float>(FactorNum) / static_cast<float>(FactorDenom);
			static_assert(FACTOR > 1.0f, "not growth if factor isn't greater than 1");

			static float log_factor(const size_type desired)
			{
				static const float inv_ln_factor = 1.0f / std::logf(FACTOR);

				if (desired <= 1) {
					return 0.0f;
				}

				return std::logf(static_cast<float>(desired)) * inv_ln_factor;
			}

			// Compute the capacity that will contain the desired number of elements while growing exponentially
			static size_type compute(const size_type desired)
			{
				const float desired_power = log_factor(desired);
				const float next_power    = std::ceilf(desired_power);

				const float next_capacity = std::floorf(std::powf(FACTOR, next_power));

				CRN_ASSERT_NO_MSG(next_capacity <= std::numeric_limits<size_type>::max());
				return static_cast<size_type>(next_capacity);
			}
		};

		// A growth profile which defines raw exponential growth of a factor defined by a rational number,
		// but limits this behavior. This profile will never request an allocation below some a pre-defined
		// value and requests above a certain capacity will grow linearly rather than exponentially.
		template <unsigned int FactorNum, unsigned int FactorDenom, size_type MinAlloc, size_type MaxExp>
		struct growth_profile_limited_exponential {
			using exponential_type = growth_profile_exponential<FactorNum, FactorDenom>;

			// Compute the capacity that will contain the desired number of elements while growing according to the profile
			static size_type compute(const size_type desired)
			{
				// Never allocate a capacity smaller than the configured limit
				if (desired <= MinAlloc) {
					return MinAlloc;
				}

				// Because memory usage is a concern, above a certain limit switch from exponential growth
				// to linear growth. This gives up the gaurantee of amortized linerar insertion times, but
				// there shouldn't be many arrays that get this large and those that do should be expected
				// to reserve an appropriate capacity.
				if (desired > MaxExp) {
					const auto mod = desired % MaxExp;
					return mod > 0
						? desired + MaxExp - mod
						: desired;
				}

				// Otherwise, exponential growth!
				return exponential_type::compute(desired);
			}
		};

		// A growth profile to use when something more specific cannot be determined
		using growth_profile_default = growth_profile_limited_exponential<3, 2, 4, 1024>;

		// An enumeration specifying the type of growth desired for a given operation
		enum class growth_profile_types : unsigned int {
			EXACT,
			DEFAULT,
		};
	}
}
