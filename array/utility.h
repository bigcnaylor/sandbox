#pragma once

#include <algorithm>
#include <type_traits>
#include "growth_profile.h"
#include "types.h"

namespace crn {
	namespace array {
		namespace utility {

			//
			// Traits

			// Determine whether or not an array implementation can re-allocate
			template <typename T, typename = std::void_t<>()>
			struct array_impl_can_reallocate : std::false_type {};

			template <typename T>
			struct array_impl_can_reallocate <
				T,
				std::void_t<
					decltype(std::declval<T>().reallocate({}, 0, 0))
				>> : std::true_type {};

			template <typename T>
			inline constexpr bool array_impl_can_reallocate_v = array_impl_can_reallocate<T>::value;

			//
			// Free Functions

			// Compute the number of blocks to allocate according to the specified growth profile
			size_type compute_new_capacity(growth_profile_types profile, size_type requested);

			// Swap the specified number of elements
			template <typename T>
			void swap_elements(T* const lhs, T* const rhs, const size_type count)
			{
				std::swap_ranges(lhs, lhs + count, rhs);
			}

			// Construct n elements from the provided arguments in the specified memory
			template <typename T, typename... Args>
			void construct_elements_from_args(void* dest, const size_type count, Args&&... args)
			{
				const auto dest_as_block = reinterpret_cast<block<T*>>(dest);
				for (size_type i = 0; i < count; ++i) {
					new (dest_as_block + i) T(std::forward<Args>(args)...);
				}
			}

			// Copy construct elements in dest from source and then delete the corresponding elements in source
			template <typename T, typename I>
			void copy_construct_elements_from_range(void* const dest, I first, const I last)
			{
				auto dest_as_block = reinterpret_cast<block<T>*>(dest);
				for (; first != last; ++first, ++dest_as_block) {
					new (dest_as_block) T(*first);
				}
			}

			template <typename T>
			void copy_construct_elements(void* const dest, T* const source, const size_type count)
			{
				if constexpr (std::is_trivially_copyable_v<T>) {
					memcpy(dest, source, count * sizeof(T));
				} else {
					copy_construct_elements_from_range<T>(dest, source, source + count);
				}
			}

			// Move construct elements in dest from source and then delete the corresponding elements in source
			template <typename T>
			void move_construct_elements(void* const dest, T* const source, const size_type count)
			{
				if constexpr (std::is_trivially_copyable_v<T>) {
					if (count > 0) {
						memcpy(dest, source, count * sizeof(T));
					}
				} else {
					const auto dest_as_block = reinterpret_cast<block<T>*>(dest);
					for (size_type i = 0; i < count; ++i) {
						new (dest_as_block + i) T(std::move(source[i]));
					}
				}
			}

			// Destroy the specified number of elements
			template <typename T>
			void destroy_elements(T* const begin, const size_type count)
			{
				if constexpr (!std::is_trivially_destructible_v<T>) {
					for (size_type i = 0; i < count; ++i) {
						begin[i].~T();
					}
				}
			}

			template <typename T>
			void copy_array_to_array(T* const dest, size_type& dest_size_in_out, const T* const source, const size_type source_size)
			{
				const size_type dest_size = dest_size_in_out;

				// Move all the elements from the right to the left that fit in the current range
				std::copy_n(source, std::min(dest_size, source_size), dest);

				// Destroy any elements in the destination beyond the range of the source
				for (size_type i = source_size; i < dest_size; ++i) {
					dest[i].~T();
				}

				// Create new elements in the destination which are moves of elements in the source beyond
				/// the range of the destination
				for (size_type i = dest_size; i < source_size; ++i) {
					new (dest + i) T(source[i]);
				}

				dest_size_in_out = source_size;
			}

			template <typename T>
			void move_array_to_array(T* const dest, size_type& dest_size_in_out, T* const source, const size_type source_size)
			{
				const size_type dest_size = dest_size_in_out;

				// Move all the elements from the right to the left that fit in the current range
				std::move(source, source + std::min(dest_size, source_size), dest);

				// Destroy any elements in the destination beyond the range of the source
				for (size_type i = source_size; i < dest_size; ++i) {
					dest[i].~T();
				}

				// Create new elements in the destination which are moves of elements in the source beyond
				/// the range of the destination
				for (size_type i = dest_size; i < source_size; ++i) {
					new (dest + i) T(std::move(source[i]));
				}

				dest_size_in_out = source_size;
			}
		}
	}
}