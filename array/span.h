#pragma once

#include <array>
#include <cassert>
#include <cstdint>
#include <iterator>
#include <memory>
#include <type_traits>
#include <vector>

#include "../core/error.h"
#include "../core/casts.h"
#include "base.h"

namespace crn {
	namespace array {

		inline constexpr size_type dynamic_extent = ~static_cast<size_type>(0);

		namespace span_detail {

			template <std::size_t N, std::size_t E>
			using enabled_if_size_is_allowed_t = std::enable_if_t<E == dynamic_extent || N == E>;

			template <typename From, typename To>
			using enabled_if_qualified_convertible_t = std::enable_if_t<is_qualification_convertible_v<From*, To*>>;
		}

		template <typename T, uint32_t E = dynamic_extent>
		class span {
		public:
			using element_type     = T;
			using value_type       = std::remove_cv_t<T>;
			using size_type        = size_type;
			using difference_type  = std::ptrdiff_t;
			using pointer          = T*;
			using const_pointer    = const T*;
			using reference        = T&;
			using const_reference  = const T&;
			using iterator         = pointer;
			using reverse_iterator = std::reverse_iterator<iterator>;

			static constexpr size_type extent = E;

			//
			// Constructor

			// default
			template <typename = std::enable_if_t<extent == 0 || extent == dynamic_extent>>
			span() :
				m_data{ nullptr },
				m_size(0)
			{
			}

			// shallow copy
			span(const span& other) = default;

			// pointer and size
			template <
				typename U,
				typename = span_detail::enabled_if_qualified_convertible_t<U, element_type>>
			span(U* const data, const size_type size) :
				m_data(data),
				m_size(size)
			{
				if constexpr (extent != dynamic_extent) {
					CRN_ASSERT_NO_MSG(size == extent);
				}
				CRN_ASSERT_NO_MSG(data != nullptr || size == 0);
			}

			// pointer range
			template <typename U, typename = span_detail::enabled_if_qualified_convertible_t<U, element_type>>
			span(U* const begin, identity_type_t<U>* const end) :
				span(begin, narrow_cast<size_type>(end - begin))
			{
			}

			// TODO: iterator and size? - how to define contiguous_iterator in C++17?
			// TODO: iterator range?

			template <
				typename U,
				size_type N,
				typename = span_detail::enabled_if_qualified_convertible_t<U, element_type>,
				typename = span_detail::enabled_if_size_is_allowed_t<N, extent>>
			span(U (&arr)[N]) :
				m_data(arr),
				m_size(N)
			{
			}

			template <
				typename U,
				size_type N,
				typename = span_detail::enabled_if_qualified_convertible_t<U, element_type>,
				typename = span_detail::enabled_if_size_is_allowed_t<N, extent>>
			span(std::array<U, N>& arr) :
				m_data(arr.data()),
				m_size(N)
			{
				static_assert(extent == dynamic_extent || extent == N);
				CRN_ASSERT_NO_MSG(data != nullptr || N == 0);
			}

			template <
				typename U,
				size_type N,
				typename = span_detail::enabled_if_qualified_convertible_t<std::add_const_t<U>, element_type>,
				typename = span_detail::enabled_if_size_is_allowed_t<N, extent>>
			span(const std::array<U, N>& arr) :
				m_data(arr.data()),
				m_size(N)
			{
				static_assert(extent == dynamic_extent || extent == N);
				CRN_ASSERT_NO_MSG(data != nullptr || N == 0);
			}

			template <
				typename U,
				size_type N,
				typename = span_detail::enabled_if_qualified_convertible_t<U, element_type>,
				typename = span_detail::enabled_if_size_is_allowed_t<N, extent>>
			span(const span<U, N>& other) :
				m_data(other.data()),
				m_size(other.size())
			{
				CRN_ASSERT_NO_MSG(extent == dynamic_extent || m_size == extent);
				CRN_ASSERT_NO_MSG(m_data != nullptr || m_size == 0);
			}

			template <
				typename U,
				typename allocator_type,
				typename = span_detail::enabled_if_qualified_convertible_t<U, element_type>>
			span(std::vector<U, allocator_type>& v) :
				span(v.data(), narrow_cast<size_type>(v.size()))
			{
			}

			template <
				typename U,
				typename allocator_type,
				typename = span_detail::enabled_if_qualified_convertible_t<std::add_const_t<U>, element_type>>
			span(const std::vector<U, allocator_type>& v) :
				span(v.data(), narrow_cast<size_type>(v.size()))
			{
			}

			template <
				typename U,
				typename Implementation,
				typename = span_detail::enabled_if_qualified_convertible_t<U, element_type>>
			span(base<U, Implementation>& arr) :
				span(arr.data(), arr.size())
			{
			}

			template <
				typename U,
				typename Implementation,
				typename = span_detail::enabled_if_qualified_convertible_t<std::add_const_t<U>, element_type>>
			span(const base<U, Implementation>& arr) :
				span(arr.data(), arr.size())
			{
			}

			//
			// Destructor

			~span() = default;

			//
			// Assignment

			// Shallow copy
			span& operator= (const span&) = default;

			//
			// Iterators

			iterator begin() const
			{
				return m_data;
			}

			iterator end() const
			{
				return m_data + m_size;
			}

			reverse_iterator rbegin() const
			{
				return std::make_reverse_iterator(begin());
			}

			reverse_iterator rend() const
			{
				return std::make_reverse_iterator(end());
			}

			//
			// Element Access

			reference front() const
			{
				CRN_ASSERT_NO_MSG(m_size > 0);
				return m_data[0];
			}

			reference back() const
			{
				CRN_ASSERT_NO_MSG(m_size > 0);
				return m_data[m_size - 1];
			}

			pointer data() const
			{
				return m_data;
			}

			reference operator[] (const size_type i) const
			{
				CRN_ASSERT_NO_MSG(i < m_size);
				return m_data[i];
			}

			//
			// Observers

			size_type size() const
			{
				return m_size;
			}

			std::size_t size_bytes() const
			{
				return m_size * sizeof(element_type);
			}

			bool empty() const
			{
				return m_size == 0;
			}

			//
			// Subspan

			template <size_type Count>
			span<element_type, Count> first() const
			{
				static_assert(Count <= extent);
				CRN_ASSERT_NO_MSG(Count <= m_size);
				return { m_data, Count };
			}

			span<element_type, dynamic_extent> first(const size_type count) const
			{
				CRN_ASSERT_NO_MSG(count <= m_size);
				return { m_data, count };
			}

			template <size_type Count>
			span<element_type, Count> last() const
			{
				static_assert(Count <= extent);
				CRN_ASSERT_NO_MSG(Count <= m_size);
				return { m_data + m_size - Count, Count };
			}

			span<element_type, dynamic_extent> last(const size_type count) const
			{
				CRN_ASSERT_NO_MSG(count <= m_size);
				return { m_data + m_size - count, count };
			}

			template <size_type Offset, size_type Count = dynamic_extent>
			auto subspan() const
			{
				static_assert(Offset <= extent);

				static constexpr size_type out_extent = compute_subspan_extent(Offset, Count);
				static_assert(out_extent <= extent);

				return make_subspan<out_extent>(Offset, out_extent);
			}

			span<element_type, dynamic_extent> subspan(const size_type offset, const size_type count = dynamic_extent) const
			{
				return make_subspan<dynamic_extent>(offset, count);
			}

		private:

			static constexpr size_type compute_subspan_extent(const size_type offset, const size_type requested_extent)
			{
				if (requested_extent != dynamic_extent) {
					return requested_extent;
				}

				if (extent == dynamic_extent) {
					return dynamic_extent;
				}

				CRN_ASSERT_NO_MSG(offset <= extent);
				return extent - offset;
			}

			template <size_type out_extent>
			span<element_type, out_extent> make_subspan(const size_type offset, const size_type count)
			{
				CRN_ASSERT_NO_MSG(offset <= m_size);
				const size_type size = count != dynamic_extent
					? count
					: m_size - offset;
				CRN_ASSERT_NO_MSG(size + offset <= m_size);

				return { m_data, size };
			}

			pointer m_data;
			size_type m_size;
		};

		//
		// Factor functions

		template <typename T>
		span<T> make_span(T* const data, const size_type size)
		{
			return { data, size };
		}

		template <typename T, size_type N>
		span<T, N> make_span(T(&arr)[N])
		{
			return { arr };
		}

		template <typename T, size_type N>
		span<T, N> make_span(std::array<T,N>& arr)
		{
			return { arr };
		}

		template <typename T, size_type N>
		span<const T, N> make_span(const std::array<T, N>& arr)
		{
			return { arr };
		}

		template <typename T, typename A>
		span<T> make_span(std::vector<T, A>& arr)
		{
			return { arr };
		}

		template <typename T, typename A>
		span<const T> make_span(const std::vector<T, A>& arr)
		{
			return { arr };
		}

		template <typename T, typename I>
		span<T> make_span(base<T, I>& arr)
		{
			return { arr };
		}

		template <typename T, typename I>
		span<const T> make_span(const base<T, I>& arr)
		{
			return { arr };
		}
	}
}