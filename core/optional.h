#pragma once

#include <algorithm>
#include <new>
#include <type_traits>
#include "error.h"
#include "type_traits.h"

namespace crn {

	// Tag to indicate an empty optional
	struct nullopt_t {
		enum class construct_t { construct_v };
		constexpr explicit nullopt_t(construct_t) {}
	};
	inline constexpr nullopt_t nullopt{nullopt_t::construct_t::construct_v};

	// Tag to indicate a value should be constructed in place
	struct inplace_t {
		enum class construct_t { construct_v };
		constexpr explicit inplace_t(construct_t) {};
	};
	inline constexpr inplace_t inplace{inplace_t::construct_t::construct_v};

	// Tag to indicate the type of policy to use when creating an optional from a factor
	template <typename P>
	struct has_value_policy_t {
		using type = P;

		enum class construct_t { construct_v };
		constexpr explicit has_value_policy_t(construct_t) {};
	};

	template <typename P>
	inline constexpr has_value_policy_t<P> has_value_policy{has_value_policy_t<P>::construct_t::construct_v};

	// Forward declaration
	template <typename T, typename HasValuePolicy>
	class optional;

	namespace opt_detail {

		// A type trait to indicate whether or not a type is constructible from an optional type
		template <typename T, typename U, typename P>
		struct constructible_from_optional {
			static constexpr bool value = std::is_constructible_v<T, optional<U, P>&>
				|| std::is_constructible_v<T, const optional<U, P>&>
				|| std::is_constructible_v<T, optional<U, P>&&>
				|| std::is_constructible_v<T, const optional<U, P>&&>;
		};

		template <typename T, typename U, typename P>
		inline constexpr bool constructible_from_optional_v = constructible_from_optional<T, U, P>::value;

		// A type trait to indicate whether or not a type is assignable from an optional type
		template <typename T, typename U, typename P>
		struct assignable_from_optional {
			static constexpr bool value = std::is_assignable_v<T, optional<U, P>&>
				|| std::is_assignable_v<T&, const optional<U, P>&>
				|| std::is_assignable_v<T&, optional<U, P>&&>
				|| std::is_assignable_v<T&, const optional<U, P>&&>;
		};

		template <typename T, typename U, typename P>
		inline constexpr bool assignable_from_optional_v = assignable_from_optional<T, U, P>::value;

		// A type trait to indicate whether or not a type is convertible from an optional type
		template <typename From, typename To, typename P>
		struct optional_convertible_to {
			static constexpr bool value = std::is_convertible_v<optional<From, P>&, To>
				|| std::is_convertible_v<const optional<From, P>&, To>
				|| std::is_convertible_v<optional<From, P>&&, To>
				|| std::is_convertible_v<const optional<From, P>&&, To>;
		};

		template <typename From, typename To, typename P>
		inline constexpr bool optional_convertible_to_v = optional_convertible_to<From, To, P>::value;

		// A type trait to indicate whether or not an optional should have assignement enabled with an optional of a different type
		template <typename T, typename U, typename P>
		struct assignment_from_optional_allowed {
			static constexpr bool value = !constructible_from_optional_v<T, U, P>
				&& !assignable_from_optional_v<T, U, P>
				&& !optional_convertible_to_v<U, T, P>;
		};

		template <typename T, typename U, typename P>
		inline constexpr bool assignment_from_optional_allowed_v = assignment_from_optional_allowed<T, U, P>::value;

		// A type trait to indicate whether or not an optional should have copy assignement enabled with an optional of a different type
		template <typename T, typename U, typename P1, typename P2>
		struct copy_convert_assignment_from_optional_allowed {
			static constexpr bool value = !std::is_same_v<optional<T, P1>, optional<U, P2>>
				&& assignment_from_optional_allowed_v<T, U, P2>
				&& std::is_constructible_v<T, std::add_const_t<T>&>
				&& std::is_assignable_v<T&, std::add_const_t<T>&>;
		};

		template <typename T, typename U, typename P1, typename P2>
		using copy_convert_assignment_from_optional_return_t = std::enable_if_t<copy_convert_assignment_from_optional_allowed<T, U, P1, P2>::value, optional<T, P1>&>;

		// A type trait to indicate whether or not an optional should have move assignement enabled with an optional of a different type
		template <typename T, typename U, typename P1, typename P2>
		struct move_convert_assignment_from_optional_allowed {
			static constexpr bool value = !std::is_same_v<optional<T, P1>, optional<U, P2>>
				&& assignment_from_optional_allowed_v<T, U, P2>
				&& std::is_constructible_v<T, U&&>
				&& std::is_assignable_v<T&, U&&>;
		};

		template <typename T, typename U, typename P1, typename P2>
		using move_convert_assignment_from_optional_return_t = std::enable_if_t<move_convert_assignment_from_optional_allowed<T, U, P1, P2>::value, optional<T, P1>&>;

		// Implement (or not) hashing an optional of the specified type
		template <typename T, typename P, typename = std::void_t<>>
		struct optional_hash_impl {
			static constexpr bool is_enabled = false;

			optional_hash_impl() = delete;
			optional_hash_impl(optional_hash_impl&&) = delete;
			~optional_hash_impl() = delete;
		};

		template <typename T, typename P>
		struct optional_hash_impl<T, P, std::void_t<std::invoke_result_t<std::hash<std::remove_cv_t<T>>, std::add_const_t<T>&>>> {
			static constexpr bool is_enabled = true;

			std::size_t operator() (const optional<T, P>& o) const
			{
				return o.has_value()
					? std::hash<std::remove_cv_t<T>>()(*o)
					: 0; // "For an optional that does not contain a value, the hash is unspecified."
			}
		};

		// Used to enable comparisons between optionals and values if the value types are comparable
		template <typename CompareResult>
		using compare_return_t = std::enable_if_t<std::is_convertible_v<CompareResult, bool>, bool>;

		// The backing memory for an optional type
		template <typename T>
		class optional_storage {
		public:
			using stored_type = std::remove_cv_t<T>;
			using data_type   = unsigned char[sizeof(T)];

			template <typename... Args>
			stored_type& construct_stored_value(Args&&... args)
			{
				const auto p_value = new (&m_data) stored_type(std::forward<Args>(args)...);
				return *p_value;
			}

			template <typename U, typename... Args>
			stored_type& construct_stored_value(std::initializer_list<U> il, Args&&... args)
			{
				const auto p_value = new (&m_data) stored_type(il, std::forward<Args>(args)...);
				return *p_value;
			}

			template <typename U>
			stored_type& assign_stored_value(U&& other)
			{
				return reinterpret_cast<stored_type&>(m_data) = std::forward<U>(other);
			}

			void destroy_stored_value()
			{
				reinterpret_cast<stored_type&>(m_data).~stored_type();
			}

			stored_type& get_stored_value()
			{
				return reinterpret_cast<stored_type&>(m_data);
			}

			const stored_type& get_stored_value() const
			{
				return reinterpret_cast<const stored_type&>(m_data);
			}

			data_type& get_data()
			{
				return m_data;
			}

			const data_type& get_data() const
			{
				return m_data;
			}

		private:
			alignas(T) data_type m_data;
		};

		// Base class to implement actual operations on the value and the associated policy for indicating whether or not there
		// is a value. This implements all the internal operations required to implement an optional type, but it does not define
		// an appropriate interface for one.
		//
		// This must be a trivial class any necessary constructors, assignment operators, or destructors will be defined or deleted by
		// a class derived from this one.
		//
		// Inherit the policy even though it could be a data member because it is possible that the policy has no data and we would like
		// to benefit from the zero-size base class optimization. Unfortunately, in the even the policy DOES have data we really want it
		// to come after the storage data (which is likely larger)...so inherit a base class which contains the storage as well.
		//
		template <typename T, typename P>
		class optional_base_common :
			private optional_storage<T>,
			private P {
		public:
			using storage_type = optional_storage<T>;
			using has_value_policy_type = P;

			static_assert(
				std::is_trivial_v<storage_type>,
				"The storage type must be trivial and shouldn't actually store anything relating to the type.");

			static_assert(
				std::is_trivial_v<has_value_policy_type>,
				"Has value policy must be trivial because it isn't factored into the determinations of optional triviallity.");


			// All of the generated member functions (constructor, destructor, and assignment) should have been
			// implemented or deleted by the appropriate base class so leave them all default here

			bool has_value_base() const
			{
				auto& data = storage_type::get_data();
				return has_value_policy_type::policy_is_engaged(data);
			}

			T& value_base()
			{
				return storage_type::get_stored_value();
			}

			const T& value_base() const
			{
				return storage_type::get_stored_value();
			}

			void reset_base()
			{
				auto& data = storage_type::get_data();
				if (has_value_policy_type::policy_is_engaged(data)) {
					storage_type::destroy_stored_value();
					has_value_policy_type::disengage_policy(data);
				}
			}

			void destroy_base()
			{
				storage_type::destroy_stored_value();
				has_value_policy_type::disengage_policy(storage_type::get_data());
			}

			void empty_construct_base()
			{
				auto& data = storage_type::get_data();
				has_value_policy_type::disengage_policy(data);
			}

			template <typename U, typename UP>
			void copy_construct_base(const optional_base_common<U, UP>& other)
			{
				if (other.has_value_base()) {
					construct_value(other.value_base());
				} else {
					empty_construct_base();
				}
			}

			template <typename U, typename UP>
			void move_construct_base(optional_base_common<U, UP>& other)
			{
				if (other.has_value_base()) {
					auto& other_value = other.value_base();
					construct_value(std::move(other_value));
				} else {
					empty_construct_base();
				}
			}

			template <typename... Args>
			void inplace_construct_base(Args&&... args)
			{
				construct_value(std::forward<Args>(args)...);
			}

			template <typename U, typename... Args>
			void inplace_construct_base(std::initializer_list<U> il, Args&&... args)
			{
				construct_value(il, std::forward<Args>(args)...);
			}

			template <typename U, typename UP>
			void copy_assign_base(const optional_base_common<U, UP>& other)
			{
				auto& self = *this;

				const bool self_has_value = self.has_value_base();
				const bool other_has_value = other.has_value_base();

				if (self_has_value && other_has_value) {
					self.assign_value(other.value_base());
				} else if (other_has_value) {
					self.construct_value(other.value_base());
				} else {
					self.reset_base();
				}
			}

			template <typename U, typename UP>
			void move_assign_base(optional_base_common<U, UP>& other)
			{
				auto& self = *this;

				const bool self_has_value = self.has_value_base();
				const bool other_has_value = other.has_value_base();

				if (self_has_value && other_has_value) {
					auto& other_value = other.value_base();
					self.assign_value(std::move(other_value));
				} else if (other_has_value) {
					auto& other_value = other.get_stored_value();
					self.construct_value(std::move(other_value));
				} else {
					self.reset_base();
				}
			}

			template <typename U>
			T& convert_assign_base(U&& other)
			{
				if (has_value_base()) {
					assign_value(std::forward<U>(other));
				} else {
					construct_value(std::forward<U>(other));
				}
			}

			template <typename... Args>
			T& emplace_base(Args&&... args)
			{
				if (has_value_base()) {
					storage_type::destroy_stored_value();
				}

				return construct_value(std::forward<Args>(args)...);
			}

			template <typename U, typename... Args>
			T& emplace_base(std::initializer_list<U> il, Args&&... args)
			{
				if (has_value_base()) {
					storage_type::destroy_stored_value();
				}

				return construct_value(il, std::forward<Args>(args)...);
			}

			template <typename P2>
			void swap_base(optional_base_common<T, P2>& rhs)
			{
				auto& lhs = *this;
			}

		private:

			template <typename U>
			void assign_value(U&& other)
			{
				storage_type::assign_stored_value(std::forward<U>(other));

				// Process the policy after construction so it is possible to verify the value is not
				// unrepresentable
				has_value_policy_type::engage_policy(storage_type::get_data());
			}

			template <typename... Args>
			T& construct_value(Args&&... args)
			{
				auto& result = storage_type::construct_stored_value(std::forward<Args>(args)...);

				// Process the policy after construction so it is possible to verify the value is not
				// unrepresentable
				has_value_policy_type::engage_policy(storage_type::get_data());

				return result;
			}

			template <typename U, typename... Args>
			T& construct_value(std::initializer_list<U> il, Args&&... args)
			{
				auto& result = storage_type::construct_stored_value(il, std::forward<Args>(args)...);

				// Process the policy after construction so it is possible to verify the value is not
				// unrepresentable
				has_value_policy_type::engage_policy(storage_type::get_data());

				return result;
			}
		};

		// Mixin-like base class to implement the correct destructor based on the type contained in an optional
		//
		template <typename Impl, typename T, bool = std::is_trivially_destructible_v<T>>
		struct optional_base_destruction_mix {
		};

		template <typename Impl, typename T>
		struct optional_base_destruction_mix<Impl, T, false> {

			~optional_base_destruction_mix()
			{
				static_cast<Impl*>(this)->reset_base();
			}
		};


		// Mixin-like base class to implement the correct copy constructor type contained in an optional
		//
		template <typename Impl, typename T, bool = std::is_copy_constructible_v<T>, bool = std::is_trivially_copy_constructible_v<T>>
		struct optional_base_copy_construction_mix {
			optional_base_copy_construction_mix() = default;
			optional_base_copy_construction_mix(const optional_base_copy_construction_mix&) = default;
		};

		template <typename Impl, typename T, bool is_trivial>
		struct optional_base_copy_construction_mix<Impl, T, false, is_trivial> {
			optional_base_copy_construction_mix() = default;
			optional_base_copy_construction_mix(const optional_base_copy_construction_mix&) = delete;
		};

		template <typename Impl, typename T>
		struct optional_base_copy_construction_mix<Impl, T, true, false> {
			optional_base_copy_construction_mix() = default;

			optional_base_copy_construction_mix(const optional_base_copy_construction_mix& other)
			{
				auto& other_as_impl = static_cast<const Impl&>(other);
				static_cast<Impl*>(this)->copy_construct_base(other);
			}
		};

		// Mixin-like base class to implement the correct move constructor type contained in an optional
		//
		template <typename Impl, typename T, bool = std::is_move_constructible_v<T>, bool = std::is_trivially_move_constructible_v<T>>
		struct optional_base_move_construction_mix {
			optional_base_move_construction_mix() = default;
			optional_base_move_construction_mix(const optional_base_move_construction_mix&) = default;
		};

		template <typename Impl, typename T, bool is_trivial>
		struct optional_base_move_construction_mix<Impl, T, false, is_trivial> {
			optional_base_move_construction_mix() = default;
			optional_base_move_construction_mix(const optional_base_move_construction_mix&) = delete;
		};

		template <typename Impl, typename T>
		struct optional_base_move_construction_mix<Impl, T, true, false> {
			optional_base_move_construction_mix() = default;

			optional_base_move_construction_mix(const optional_base_move_construction_mix& other)
			{
				auto& other_as_impl = static_cast<const Impl&>(other);
				static_cast<Impl*>(this)->move_construct_base(other);
			}
		};

		// Mixin-like base class to implement the correct copy assignment operator based on the type contained in an optional
		//
		template <
			typename Impl,
			typename T,
			bool = std::is_copy_assignable_v<T> && std::is_copy_constructible_v<T>,
			bool = std::is_trivially_copy_assignable_v<T>&& std::is_trivially_copy_constructible_v<T>>
		struct optional_base_copy_assignment_mix {
			optional_base_copy_assignment_mix& operator= (const optional_base_copy_assignment_mix&) = default;
		};

		template <typename Impl, typename T, bool is_trivial>
		struct optional_base_copy_assignment_mix<Impl, T, false, is_trivial> {
			optional_base_copy_assignment_mix& operator= (const optional_base_copy_assignment_mix&) = delete;
		};

		template <typename Impl, typename T>
		struct optional_base_copy_assignment_mix<Impl, T, true, false> {

			optional_base_copy_assignment_mix& operator= (const optional_base_copy_assignment_mix& other)
			{
				auto& other_as_impl = static_cast<const Impl&>(other);
				static_cast<Impl*>(this)->copy_assign_base(other_as_impl);
				return *this;
			}
		};

		// Mixin-like base class to implement the correct move assignment operator based on the type contained in an optional
		//
		template <
			typename Impl,
			typename T,
			bool = std::is_move_assignable_v<T>&& std::is_move_constructible_v<T>,
			bool = std::is_trivially_move_assignable_v<T>&& std::is_trivially_move_constructible_v<T>>
		struct optional_base_move_assignment_mix {
			optional_base_move_assignment_mix& operator= (const optional_base_move_assignment_mix&) = default;
		};

		template <typename Impl, typename T, bool is_trivial>
		struct optional_base_move_assignment_mix<Impl, T, false, is_trivial> {
			optional_base_move_assignment_mix& operator= (const optional_base_move_assignment_mix&) = delete;
		};

		template <typename Impl, typename T>
		struct optional_base_move_assignment_mix<Impl, T, true, false> {

			optional_base_move_assignment_mix& operator= (const optional_base_move_assignment_mix& other)
			{
				auto& other_as_impl = static_cast<const Impl&>(other);
				static_cast<Impl*>(this)->move_assign_base(other_as_impl);
				return *this;
			}
		};

		// Put it all together now! This is the base class for the optional type to PRIVATELY inherit. It has
		// all the necessary data, implements all the required operations, AND has the appropriate destructor,
		// copy/move construction, and copy/move assignment
		//
		template <typename T, typename P>
		class optional_base :
			public optional_base_common<T, P>,
			public optional_base_destruction_mix<optional_base<T, P>, T>,
			public optional_base_copy_construction_mix<optional_base<T, P>, T>,
			public optional_base_move_construction_mix<optional_base<T, P>, T>,
			public optional_base_copy_assignment_mix<optional_base<T, P>, T>,
			public optional_base_move_assignment_mix<optional_base<T, P>, T> {
		};
	}

	// An optional policy which simply tracks a Boolean flag to determine if the optional is engaged
	class optional_has_value_policy_flag {
	public:

		bool policy_is_engaged(const unsigned char*) const
		{
			return m_is_engaged;
		}

		void engage_policy(unsigned char*)
		{
			m_is_engaged = true;
		}

		void disengage_policy(unsigned char*)
		{
			m_is_engaged = false;
		}

	private:
		bool m_is_engaged;
	};

	// An optional policy which uses all bits set to indicate there is no value
	//
	// This is more memory efficient, but means at least 1 value is unrepresentable and
	// accesses more memory on a check
	//
	class optional_has_value_policy_full_bits {
	public:

		template <std::size_t N>
		static bool policy_is_engaged(const unsigned char (&data)[N])
		{
			// Check as many bytes as we can with the 
			if constexpr ((N % sizeof(std::uint64_t)) == 0) {
				return is_engaged_impl<std::uint64_t>(data);
			} else if constexpr ((N % sizeof(std::uint32_t)) == 0) {
				return is_engaged_impl<std::uint32_t>(data);
			} else if constexpr ((N % sizeof(std::uint16_t)) == 0) {
				return is_engaged_impl<std::uint16_t>(data);
			} else {
				return is_engaged_impl<unsigned char>(data);
			}
		}

		template <std::size_t N>
		static void engage_policy(unsigned char (&data)[N])
		{
			CRN_ASSERT(
				policy_is_engaged(data),
				"An optional using the full bits policy has been set to the unrepresentatible value.");
		}

		template <std::size_t N>
		static void disengage_policy(unsigned char (&data)[N])
		{
			// Set as many bytes as we can with the 
			if constexpr ((N % sizeof(std::uint64_t)) == 0) {
				return engage_policy_impl<std::uint64_t>(data);
			} else if constexpr ((N % sizeof(std::uint32_t)) == 0) {
				return engage_policy_impl<std::uint32_t>(data);
			} else if constexpr ((N % sizeof(std::uint16_t)) == 0) {
				return engage_policy_impl<std::uint16_t>(data);
			} else {
				return engage_policy_impl<unsigned char>(data);
			}
		}

	private:

		template <typename I, std::size_t N>
		static bool is_engaged_impl(const unsigned char(&data)[N])
		{
			static_assert(std::is_integral_v<I> && std::is_unsigned_v<I>);
			static_assert(N % sizeof(I) == 0);

			const auto begin = reinterpret_cast<const I*>(data);
			const auto end   = begin + (N / sizeof(I));
			return std::all_of(begin, end, [] (const I v) { return v == std::numeric_limits<I>::max(); });
		}

		template <typename I, std::size_t N>
		static void engage_policy_impl(unsigned char(&data)[N])
		{
			static_assert(std::is_integral_v<I> && std::is_unsigned_v<I>);
			static_assert(N % sizeof(I) == 0);

			const auto begin = reinterpret_cast<I*>(data);
			const auto end = begin + (N / sizeof(I));
			std::fill(begin, end, std::numeric_limits<I>::max());
		}
	};

	// A type which mary or may not contain a value
	template <typename T, typename HasValuePolicy = optional_has_value_policy_flag>
	class optional : private opt_detail::optional_base<T, HasValuePolicy> {
	public:
		//
		// Types

		using value_type = T;
		using own_type   = optional<T, HasValuePolicy>;

		static_assert(!std::is_reference_v<T>, "Optional values may not be references.");
		static_assert(std::is_destructible_v<T>, "Optional values must be destructible.");
		static_assert(!std::is_same_v<own_type, inplace_t>, "Optional may not contain the inplace type tag.");

		// Make optionals friends to allow conversions to the template base class
		template <typename U, typename P> friend class optional;

		//
		// Constructors

		// Disengaged constructors
		optional()
		{
			own_type::empty_construct_base();
		}

		optional(nullopt_t)
		{
			own_type::empty_construct_base();
		}

		// Copy/Move

		// The following are either implemented, trivial, or deleted, according to the contained type:
		//   optional(const optional& other);
		//   optional(optional&& other);

		template <
			typename U,
			typename P,
			std::enable_if_t<std::is_constructible_v<T, std::add_const_t<U>&>
				&& std::is_convertible_v<std::add_const_t<U>&, T>
				&& !std::is_same_v<optional<U, P>, own_type>>* = nullptr>
		optional(const optional<U, P>& other)
		{
			own_type::copy_construct_base(other);
		}

		template <
			typename U,
			typename P,
			std::enable_if_t<std::is_constructible_v<T, std::add_const_t<U>&>
				&& !std::is_convertible_v<std::add_const_t<U>&, T>
				&& !std::is_same_v<optional<U, P>, own_type>>* = nullptr>
		explicit optional(const optional<U, P>& other)
		{
			own_type::copy_construct_base(other);
		}

		template <
			typename U,
			typename P,
			std::enable_if_t<std::is_constructible_v<T, U&&>
				&& std::is_convertible_v<U&&, T>>* = nullptr>
		optional(optional<U, P>&& other)
		{
			own_type::move_construct_base(other);
		}

		template <
			typename U,
			typename P,
			std::enable_if_t<std::is_constructible_v<T, U&&>
				&& !std::is_convertible_v<U&&, T>>* = nullptr>
		explicit optional(optional<U, P>&& other)
		{
			own_type::move_construct_base(other);
		}

		// Enaged constructors
		template <
			typename U = T,
			std::enable_if_t<std::is_convertible_v<U&&, T>
				&& std::is_constructible_v<T, U&&>
				&& !std::is_same_v<optional, remove_cvref_t<U>>
				&& !std::is_same_v<inplace_t, remove_cvref_t<U>>>* = nullptr>
		optional(U&& other)
		{
			own_type::inplace_construct_base(std::forward<U>(other));
		}

		template <
			typename U = T,
			std::enable_if_t<!std::is_convertible_v<U&&, T>
				&& std::is_constructible_v<T, U&&>
				&& !std::is_same_v<optional, remove_cvref_t<U>>
				&& !std::is_same_v<inplace_t, remove_cvref_t<U>>>* = nullptr>
		explicit optional(U&& other)
		{
			own_type::inplace_construct_base(std::forward<U>(other));
		}

		template <
			typename... Args,
			std::enable_if_t<std::is_constructible_v<T, Args&&...>>* = nullptr>
		explicit optional(inplace_t, Args&&... args)
		{
			own_type::inplace_construct_base(std::forward<Args>(args)...);
		}

		template <
			typename U,
			typename... Args,
			std::enable_if_t<std::is_constructible_v<T, std::initializer_list<U>&, Args&&...>>* = nullptr>
		explicit optional(inplace_t, std::initializer_list<U> il, Args&&... args)
		{
			own_type::inplace_construct_base(il, std::forward<Args>(args)...);
		}

		//
		// Destructor

		// This is either implemented or trivial according to the contained type:
		//   ~optional();

		//
		// Assignment

		own_type& operator=(nullopt_t)
		{
			own_type::reset_base();
			return *this;
		}

		// Copy / Move

		// The following are either implemented, trivial, or deleted, according to the contained type:
		//   optional& operator= (const optional& other);
		//   optional& operator= (optional&& other);

		template <typename U, typename P>
		auto operator= (const optional<U, P>& other) -> opt_detail::copy_convert_assignment_from_optional_return_t<T, U, HasValuePolicy, P>
		{
			own_type::copy_assign_base(other);
			return *this;
		}

		template <typename U, typename P>
		auto operator= (optional<U, P>&& other) -> opt_detail::move_convert_assignment_from_optional_return_t<T, U, HasValuePolicy, P>
		{
			own_type::move_assign_base(other);
			return *this;
		}

		// From value
		template <typename U = T>
		auto operator= (U&& other) -> std::enable_if_t<std::is_constructible_v<T, U&&> && std::is_assignable_v<T, U&&>, own_type&>
		{
			own_type::convert_assign_base(std::forward<U>(other));
			return *this;
		}

		//
		// Observers

		// Pointer/Reference to value (unchecked)
		T* operator->()
		{
			CRN_ASSERT_NO_MSG(own_type::has_value_base());
			auto& value = own_type::value_base();
			return &value;
		}

		std::add_const_t<T>* operator->() const
		{
			CRN_ASSERT_NO_MSG(own_type::has_value_base());
			auto& value = own_type::value_base();
			return &value;
		}

		T& operator*() &
		{
			CRN_ASSERT_NO_MSG(own_type::has_value_base());
			return own_type::value_base();
		}

		std::add_const_t<T>& operator*() const&
		{
			CRN_ASSERT_NO_MSG(own_type::has_value_base());
			return own_type::value_base();
		}

		T&& operator*() &&
		{
			CRN_ASSERT_NO_MSG(own_type::has_value_base());
			auto& value = own_type::value_base();
			return std::move(value);
		}

		std::add_const_t<T>&& operator*() const&&
		{
			CRN_ASSERT_NO_MSG(own_type::has_value_base());
			auto& value = own_type::value_base();
			return std::move(value);
		}

		// Status of the optional value
		explicit operator bool() const
		{
			return own_type::has_value_base();
		}

		bool has_value() const
		{
			return own_type::has_value_base();
		}

		// Reference to value (checked - use fatal error instead of exception)
		T& value() &
		{
			if (!own_type::has_value_base()) {
				CRN_FATAL_ERROR("Bad optional access.");
			}

			return own_type::get_value();
		}

		std::add_const_t<T>& value() const&
		{
			if (!own_type::has_value_base()) {
				CRN_FATAL_ERROR("Bad optional access.");
			}

			return own_type::get_value();
		}

		T&& value() &&
		{
			if (!own_type::has_value_base()) {
				CRN_FATAL_ERROR("Bad optional access.");
			}

			auto& value = own_type::get_value();
			return std::move(value);
		}

		std::add_const_t<T>&& value() const&&
		{
			if (!own_type::has_value_base()) {
				CRN_FATAL_ERROR("Bad optional access.");
			}

			auto& value = own_type::get_value();
			return std::move(value);
		}

		// Conditional access to value
		template <typename U>
		auto value_or(U&& default_value) const& -> std::enable_if_t<std::is_convertible_v<U&&, T> && std::is_copy_constructible_v<T>, T>
		{
			return own_type::has_value_base()
				? own_type::get_value()
				: static_cast<T>(std::forward<U>(default_value));
		}

		template <typename U>
		auto value_or(U&& default_value) && -> std::enable_if_t<std::is_convertible_v<U&&, T>&& std::is_move_constructible_v<T>, T>
		{
			return own_type::has_value_base()
				? std::move(own_type::get_value())
				: static_cast<T>(std::forward<U>(default_value));
		}

		//
		// Modifiers

		template <typename P2>
		void swap(optional<T, P2>& rhs)
		{
			own_type& lhs = *this;

			const bool have_left  = lhs.has_value_base();
			const bool have_right = rhs.has_value_base();

			if (have_left && have_right) {
				using std::swap;
				swap(lhs.value_base(), rhs.value_base());
			} else if (have_left) {
				auto& lhs_value = lhs.value_base();
				rhs.inplace_construct_base(std::move(lhs_value));
				lhs.destroy_base();
			} else if (have_right) {
				auto& rhs_value = rhs.value_base();
				lhs.inplace_construct_base(std::move(rhs_value));
				rhs.destroy_base();
			}
		}

		void reset()
		{
			own_type::reset_base();
		}

		template <typename... Args>
		auto emplace(Args&&... args) -> std::enable_if_t<std::is_constructible_v<T, Args&&...>, T&>
		{
			return own_type::emplace_base(std::forward<Args>(args)...);
		}

		template <typename U, typename... Args>
		auto emplace(std::initializer_list<U> il, Args&&... args) -> std::enable_if_t<std::is_constructible_v<T, std::initializer_list<U>&, Args&&...>, T&>
		{
			return own_type::emplace_base(il, std::forward<Args>(args)...);
		}
	};

	template <typename T>
	using compact_optional = optional<T, optional_has_value_policy_full_bits>;

	//
	// Deduction guides

	template <typename T> optional(T) -> optional<T>;

	//
	// Customization points

	template <typename T, typename P1, typename P2>
	void swap(optional<T, P1>& lhs, optional<T, P2>& rhs)
	{
		lhs.swap(rhs);
	}

	//
	// Factory functions

	template <typename T>
	optional<std::decay_t<T>> make_optional(T&& v)
	{
		return optional<std::decay_t<T>>(std::forward<T>(v));
	}

	template <typename T, typename HasValuePolicy>
	optional<std::decay_t<T>, HasValuePolicy> make_optional(has_value_policy_t<HasValuePolicy>, T&& v)
	{
		return optional<std::decay_t<T>, HasValuePolicy>(std::forward<T>(v));
	}

	template <typename T, typename... Args>
	optional<T> make_optional(Args&&... args)
	{
		return optional<T>(inplace, std::forward<Args>(args)...);
	}

	template <typename T, typename HasValuePolicy, typename... Args>
	optional<T, HasValuePolicy> make_optional(Args&&... args)
	{
		return optional<T, HasValuePolicy>(inplace, std::forward<Args>(args)...);
	}

	template <typename T, typename U, typename... Args>
	optional<T> make_optional(std::initializer_list<U> il, Args&&... args)
	{
		return optional<T>(inplace, il, std::forward<Args>(args)...);
	}

	template <typename T, typename HasValuePolicy, typename U, typename... Args>
	optional<T, HasValuePolicy> make_optional(std::initializer_list<U> il, Args&&... args)
	{
		return optional<T, HasValuePolicy>(inplace, il, std::forward<Args>(args)...);
	}

	//
	// Comparisons

	// Comparison between 2 optionals
	template <typename T, typename U, typename P1, typename P2>
	auto operator== (const optional<T, P1>& lhs, const optional<U, P2>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() == std::declval<U>())>
	{
		return lhs.has_value() == rhs.has_value()
			&& (!lhs.has_value() || *lhs == *rhs);
	}

	template <typename T, typename U, typename P1, typename P2>
	auto operator!= (const optional<T, P1>& lhs, const optional<U, P2>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() != std::declval<U>())>
	{
		return lhs.has_value() != rhs.has_value()
			|| (lhs.has_value() && *lhs != *rhs);
	}

	template <typename T, typename U, typename P1, typename P2>
	auto operator< (const optional<T, P1>& lhs, const optional<U, P2>& rhs)->opt_detail::compare_return_t<decltype(std::declval<T>() < std::declval<U>())>
	{
		return rhs.has_value()
			&& (!lhs.has_value() || *lhs < *rhs);
	}

	template <typename T, typename U, typename P1, typename P2>
	auto operator<= (const optional<T, P1>& lhs, const optional<U, P2>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() <= std::declval<U>())>
	{
		return !lhs.has_value()
			|| (rhs.has_value() && *lhs <= *rhs);
	}

	template <typename T, typename U, typename P1, typename P2>
	auto operator> (const optional<T, P1>& lhs, const optional<U, P2>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() > std::declval<U>())>
	{
		return lhs.has_value()
			&& (!rhs.has_value() || *lhs > *rhs);
	}

	template <typename T, typename U, typename P1, typename P2>
	auto operator>= (const optional<T, P1>& lhs, const optional<U, P2>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() >= std::declval<U>())>
	{
		return !rhs.has_value()
			|| (lhs.has_value() && *lhs >= *rhs);
	}

	// Comparison with nullopt
	template <typename T, typename P>
	bool operator== (const optional<T, P>& lhs, nullopt_t)
	{
		return !lhs.has_value();
	}

	template <typename T, typename P>
	bool operator== (nullopt_t, const optional<T, P>& rhs)
	{
		return !rhs.has_value();
	}

	template <typename T, typename P>
	bool operator!= (const optional<T, P>& lhs, nullopt_t)
	{
		return lhs.has_value();
	}

	template <typename T, typename P>
	bool operator!= (nullopt_t, const optional<T, P>& rhs)
	{
		return rhs.has_value();
	}

	template <typename T, typename P>
	bool operator< (const optional<T, P>& lhs, nullopt_t)
	{
		return false;
	}

	template <typename T, typename P>
	bool operator< (nullopt_t, const optional<T, P>& rhs)
	{
		return rhs.has_value();
	}

	template <typename T, typename P>
	bool operator<= (const optional<T, P>& lhs, nullopt_t)
	{
		return !lhs.has_value();
	}

	template <typename T, typename P>
	bool operator<= (nullopt_t, const optional<T, P>& rhs)
	{
		return !rhs.has_value();
	}

	template <typename T, typename P>
	bool operator> (const optional<T, P>& lhs, nullopt_t)
	{
		return lhs.has_value();
	}

	template <typename T, typename P>
	bool operator> (nullopt_t, const optional<T, P>& rhs)
	{
		return false;
	}

	template <typename T, typename P>
	bool operator>= (const optional<T, P>& lhs, nullopt_t)
	{
		return !lhs.has_value();
	}

	template <typename T, typename P>
	bool operator>= (nullopt_t, const optional<T, P>& rhs)
	{
		return !rhs.has_value();
	}

	// Comparison between optional and a value
	template <typename T, typename U, typename P>
	auto operator== (const optional<T, P>& lhs, const U& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() == std::declval<U>())>
	{
		return lhs.has_value() && (*lhs == rhs);
	}

	template <typename T, typename U, typename P>
	auto operator== (const T& lhs, const optional<U, P>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() == std::declval<U>())>
	{
		return rhs.has_value() && (lhs == *rhs);
	}

	template <typename T, typename U, typename P>
	auto operator!= (const optional<T, P>& lhs, const U& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() != std::declval<U>())>
	{
		return !lhs.has_value() || (*lhs != rhs);
	}

	template <typename T, typename U, typename P>
	auto operator!= (const T& lhs, const optional<U, P>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() != std::declval<U>())>
	{
		return !rhs.has_value() || (lhs != *rhs);
	}

	template <typename T, typename U, typename P>
	auto operator< (const optional<T, P>& lhs, const U& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() < std::declval<U>())>
	{
		return !lhs.has_value() || (*lhs < rhs);
	}

	template <typename T, typename U, typename P>
	auto operator< (const T& lhs, const optional<U, P>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() < std::declval<U>())>
	{
		return rhs.has_value() && (lhs < *rhs);
	}

	template <typename T, typename U, typename P>
	auto operator<= (const optional<T, P>& lhs, const U& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() <= std::declval<U>())>
	{
		return !lhs.has_value() || (*lhs <= rhs);
	}

	template <typename T, typename U, typename P>
	auto operator<= (const T& lhs, const optional<U, P>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() <= std::declval<U>())>
	{
		return rhs.has_value() && (lhs <= *rhs);
	}

	template <typename T, typename U, typename P>
	auto operator> (const optional<T, P>& lhs, const U& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() > std::declval<U>())>
	{
		return lhs.has_value() && (*lhs > rhs);
	}

	template <typename T, typename U, typename P>
	auto operator> (const T& lhs, const optional<U, P>& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() > std::declval<U>())>
	{
		return !rhs.has_value() || (lhs > *rhs);
	}

	template <typename T, typename U, typename P>
	auto operator>= (const optional<T, P>& lhs, const U& rhs) -> opt_detail::compare_return_t<decltype(std::declval<T>() >= std::declval<U>())>
	{
		return lhs.has_value() || (*lhs >= rhs);
	}

	template <typename T, typename U, typename P>
	auto operator>= (const T& lhs, const optional<U, P>& rhs)->opt_detail::compare_return_t<decltype(std::declval<T>() >= std::declval<U>())>
	{
		return !rhs.has_value() || (lhs >= *rhs);
	}
}

// Specialization of std::hash
template <typename T, typename P>
struct std::hash<crn::optional<T, P>> : crn::opt_detail::optional_hash_impl<T, P> {};