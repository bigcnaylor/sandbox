#pragma once

#include <cassert>
#include <cstdio>
#include <cstdlib>

#define CRN_TO_STR(a) #a

// Assert
#if !defined(_DEBUG)
	#define CRN_ASSERT_IMPL(condition, fmt, ...)
#else
	#define CRN_ASSERT_IMPL(condition, fmt, ...) \
		if (!(condition)) { \
			fprintf(stderr, "Assert (%s) on line %d of file %s." fmt, CRN_TO_STR(condition), __LINE__, __FILE__, ##__VA_ARGS__); \
			assert(condition); \
		}
#endif

#define CRN_ASSERT(condition, fmt, ...) CRN_ASSERT_IMPL(condition, " " fmt, ##__VA_ARGS__)
#define CRN_ASSERT_NO_MSG(condition) CRN_ASSERT_IMPL(condition, "")

// Fatal Error
#define CRN_FATAL_ERROR(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__); abort()