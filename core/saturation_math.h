#pragma once

#include <climits>
#include <cstdint>
#include <limits>
#include <type_traits>
#include "type_traits.h"

#if defined(__SIZEOF_INT128__)
	#define CRN_SATMATH_HAS_INT128() 1
#else
	#define CRN_SATMATH_HAS_INT128() 0
#endif

#if !CRN_SATMATH_HAS_INT128() &&  defined(_MSC_VER)
	#include <intrin.h>
	#define CRN_SATMATH_MUL128() 1
#else
	#define CRN_SATMATH_MUL128() 0
#endif

namespace crn {

	// This is a collection of aturating integer arithmetic operations. In theory these should
	// work on all integer types up to 64 bits wide, but supporting multiplication on 64-bit integers is
	// dependent on non-standard types and/or compiler intrinsics so watch out for that. Potential branches
	// have been kept to a minimum, but cannot be completely avoided in C++. The good news is that compilers
	// are pretty smart and can often avoid these branches, and (since we've minimized them) maybe when they
	// can't it is for the best.
	//
	// The algorithms below were adapted from https://locklessinc.com/articles/sat_arithmetic/

	namespace saturation_math {

		namespace detail {

			// SFINAE helpers to only have a function participate in overload resolution for signed or unsigned integers
			template <typename T>
			using return_if_signed_int_t = std::enable_if_t<std::is_integral_v<std::remove_reference_t<T>> && std::is_signed_v<std::remove_reference_t<T>>, T>;

			template <typename T>
			using return_if_unsigned_int_t = std::enable_if_t<std::is_integral_v<std::remove_reference_t<T>> && std::is_unsigned_v<std::remove_reference_t<T>>, T>;

			// Type trait to determine the next largest integer type
			template <typename T> struct next_size;
			template <> struct next_size<uint8_t>  : identity_type<uint16_t> {};
			template <> struct next_size<uint16_t> : identity_type<uint32_t> {};
			template <> struct next_size<uint32_t> : identity_type<uint64_t> {};
			template <> struct next_size<int8_t>   : identity_type<int16_t> {};
			template <> struct next_size<int16_t>  : identity_type<int32_t> {};
			template <> struct next_size<int32_t>  : identity_type<int64_t> {};

#if CRN_SATMATH_HAS_INT128()
			template <> struct next_size<uint64_t> : identity_type<unsigned __int128> {};
			template <> struct next_size<int64_t>  : identity_type<__int128> {};
#endif

			template <typename T> using next_size_t = typename next_size<T>::type;

			// The number of bits in a type
			template <typename T>
			inline constexpr std::size_t bit_size_v = sizeof(T) * CHAR_BIT;

			// The number of bits to shift in order to obtain the sign bit (bit_size - 1)
			template <typename T>
			inline constexpr std::size_t sign_bit_shift_v = bit_size_v<T> - 1;
		}

		//
		// Addition

		// Unsigned
		template <typename T>
		constexpr auto add(const T lhs, const T rhs) -> detail::return_if_unsigned_int_t<T>
		{
			// Underflow is not possible with unsigned integers, so we only need to worry about overflow. If result is less
			// than either of the inputs overflow occurred. To set the result high in this case this without branching we:
			//   1. Get an integer representation of result < either input (only need to compare one of them). This will be either
			//      0 (false) or 1 (true).
			//   2. Negate the result of the comparison so that it is either 0 (false) or all high bits (true). This works because there
			//      is no -0 and -1 causes underflow of an unsigned type and sets all the bits high.
			//   3. Bitwise OR with the result. In the case of overflow (true) all the bits will get set high and in the normal
			//      case (false) on the bits that were set in the result will be set

			const T res = lhs + rhs;
			return res | -static_cast<T>(res < lhs);
		}

		// Signed
		template <typename T>
		constexpr auto add(const T lhs, const T rhs) -> detail::return_if_signed_int_t<T>
		{
			// Start by performing the operation on unsigned integers
			using u_type = std::make_unsigned_t<T>;

			const u_type ulhs = lhs;
			const u_type urhs = rhs;
			const u_type ures = ulhs + urhs;

			// Overflow can only occur if both inputs have the same sign. This can be checked by comparing the highest bit
			// in both inputs, if they are equal they have the same sign: ((ulhs ^ urhs) >> (bit_size - 1)) == 0.
			//
			// Given this, overflow HAS occured if the result has the opposite sign of the inputs (which must have the
			// same sign at this point: ((urhs ^ ures) >> (bit_size - 1)) == 1
			//
			// Putting this altogether:
			//  ((ulhs ^ urhs) >> (bit_size<T> - 1)) == 0 && ((ulhs ^ ures) >> (bit_size<T> - 1)) == 1
			//  !((ulhs ^ urhs) >> (bit_size<T> - 1)) && ((ulhs ^ ures) >> (bit_size<T> - 1))
			//  (~(ulhs ^ urhs) >> (bit_size<T> - 1)) && ((ulhs ^ ures) >> (bit_size<T> - 1))
			//  (~(ulhs ^ urhs) & (ulhs ^ ures)) >> (bit_size - 1)
			//
			// The bit shift can be ommitted if the unsigned result is converted to a signed integer. The the highest bit
			// in the result of the expression is 1 a signed twos complement integer will be negative. So overflow has occurred if:
			//  static_cast<T>(~(ulhs ^ urhs) & (ulhs ^ ures)) < 0
			//
			// Alternatively, if we flip the comparison (that's what the linked article does, but why?):
			//  static_cast<T>((ulhs ^ urhs) | ~(ulhs ^ ures)) >= 0

			// Now all that is left is to compute the result to return in the event of overflow. Since we can reasonably assume
			// that signed integers are two's compelment (required in C++20?), add 1 to the max integer if the same-sign inputs
			// are negative and zero if positive. EASY:
			//  (std::numeric_limits<T>::max() + (ulhs >> (bit_size<T> - 1)))

			// Looks like a branch, but the compiler should perform a conditional move instead. However,
			// this does mean we alway compute the theoretical result of an overflow.
			const u_type overflow_result = std::numeric_limits<T>::max() + (ulhs >> detail::sign_bit_shift_v<T>);
			return static_cast<T>(~(ulhs ^ urhs) & (ulhs ^ ures)) < 0 ? overflow_result : ures;
		}

		//
		// Increment

		// Unsigned
		template <typename T>
		auto inc(T& v) -> detail::return_if_unsigned_int_t<T&>
		{
			// Increment is a special case of addition, so the steps are basically the same, but use the built-in operator
			// and take the value as input/output

			++v;
			v |= -static_cast<T>(v < 1);
			return v;
		}

		// Signed
		template <typename T>
		auto inc(T& v) -> detail::return_if_signed_int_t<T&>
		{
			using u_type = std::make_unsigned_t<T>;

			// A simplified version of addition. See notes there. Basically, the check is simplified because we know:
			//   1. rhs is always positive
			//   2. underflow is not possible
			//   3. overflow is only possible when lhs (v) is positive and has ocurred when res is negative
			u_type ures = v;
			const u_type ulhs = ures++;
			v = static_cast<T>((~ulhs) & ures) < 0 ? std::numeric_limits<T>::max() : ures;
			return v;
		}

		//
		// Subtraction

		// Unsigned
		template <typename T>
		constexpr auto sub(const T lhs, const T rhs) -> detail::return_if_unsigned_int_t<T>
		{
			// Overflow is not possible with unsigned integers. If the result is greater than the left input then overflow occurred.
			// To set all the bits low in this case without branching we:
			//   1. Get an integer representation of the result <= the left input. This will be either 0 (false) or 1 (true)
			//   2. Negate the result of this comparison so that it is either 0 (false) or all high bits (true). This works because there
			//      is no -0 and -1 causes underflow of an unsigned type and sets all the bits high.
			//   3. Bitwise AND with the result. In the case of underflow (false) no bits will be set in the result and in the normal
			//      (true) case only the bits that were set in the result will be set.

			const T res = lhs - rhs;
			return res & -static_cast<T>(res <= lhs);
		}

		// Signed
		template <typename T>
		constexpr auto sub(const T lhs, const T rhs) -> detail::return_if_signed_int_t<T>
		{
			// Start by performing the operation on unsigned integers
			using u_type = std::make_unsigned_t<T>;

			const u_type ulhs = lhs;
			const u_type urhs = rhs;
			const u_type ures = ulhs - urhs;

			// Overflow can only occur if both inputs have a different sign. This can be checked by comparing the highest bit
			// in both inputs, if they are not equal they have a different sign: ((ulhs ^ urhs) >> (bit_size - 1)) == 1.
			//
			// Given this, overflow HAS occured if the result has the opposite sign of the left input: ((ulhs ^ ures) >> (bit_size - 1)) == 1
			//
			// Putting this altogether:
			//  ((ulhs ^ urhs) >> (bit_size<T> - 1)) == 1 && ((ulhs ^ ures) >> (bit_size<T> - 1)) == 1
			//  ((ulhs ^ urhs) >> (bit_size<T> - 1)) && ((ulhs ^ ures) >> (bit_size<T> - 1))
			//  ((ulhs ^ urhs) & (ulhs ^ ures)) >> (bit_size - 1)
			//
			// The bit shift can be ommitted if the unsigned result is converted to a signed integer. The the highest bit
			// in the result of the expression is 1 a signed twos complement integer will be negative. So overflow has occurred if:
			//  static_cast<T>((ulhs ^ urhs) & (ulhs ^ ures)) < 0

			// Now all that is left is to compute the result to return in the event of overflow. Since we can reasonably assume
			// that signed integers are two's compelment (required in C++20?), add 1 to the max integer if the left input is
			// negative and zero if positive. EASY:
			//  (std::numeric_limits<T>::max() + (ulhs >> (bit_size<T> - 1)))

			// Looks like a branch, but the compiler should perform a conditional move instead. However,
			// this does mean we alway compute the theoretical result of an overflow.
			const u_type overflow_result = std::numeric_limits<T>::max() + (ulhs >> detail::sign_bit_shift_v<T>);
			return static_cast<T>((ulhs ^ urhs) & (ulhs ^ ures)) < 0 ? overflow_result : ures;
		}

		//
		// Decrement

		// Unsigned
		template <typename T>
		auto dec(T& v) -> detail::return_if_unsigned_int_t<T&>
		{
			// Decrement is a special case of subtraction, so the steps are basically the same, but use the built-in operator
			// and take the value as input/output

			const T pre_v = v--;
			v &= -static_cast<T>(v <= pre_v);
			return v;
		}

		// Signed
		template <typename T>
		auto dec(T& v) -> detail::return_if_signed_int_t<T&>
		{
			using u_type = std::make_unsigned_t<T>;

			// A simplified version of subtraction. See notes there. Basically, the check is simplified because we know:
			//   1. rhs is always positive
			//   3. overflow is not possible
			//   2. underflow is only possible when lhs (v) is negative and has occurred if res is zero or positive
			u_type ures = v;
			const u_type ulhs = ures--;
			v = static_cast<T>(ulhs & (~ures)) < 0 ? std::numeric_limits<T>::min() : ures;
			return v;
		}

		//
		// Multiplication

		// Unsigned
		template <typename T>
		constexpr auto mul(const T lhs, const T rhs) -> detail::return_if_unsigned_int_t<T>
		{
			// Underflow is not possible with multiplication of integers. In order to accurately check for overflow multiplication
			// needs to upconvert to a larger integer type and check the bits that are beyond the range of the input type.
			//   1. Optain the result of multiplication on upconverted types
			//   2. Split the result into high (out of range) and low (usable) bits
			//   3. Use the !! "pseudo-operator" to get a value of 0 if the high bits are empty and 1 if the high bits have any value
			//   4. Negate this result so that it is either 0 (false) or all high bits (true). This works because there is no -0 and -1
			//      causes underflow of an unsigned type and sets all the bits high.
			//   5. Bitwise OR with the result. In the case of overflow (true) all bits will be set in the result and in the normal
			//      (false) case only the bits that were set in the low bits will be set.

			using large_type = detail::next_size_t<T>;

			const large_type res = static_cast<large_type>(lhs) * static_cast<large_type>(rhs);

			const auto high = static_cast<T>(res >> detail::bit_size_v<T>);
			const auto low  = static_cast<T>(res);

			return low | -static_cast<T>(!!high);
		}

#if CRN_SATMATH_MUL128()

		template <>
		uint64_t mul<uint64_t>(const uint64_t lhs, const uint64_t rhs)
		{
			// Speciallize the uint64 case because neither standard C++ nor MSVC has an 128 bit integer to use as the large type. Use
			// a compiler intrinsic instead.
			uint64_t high;
			const uint64_t low = _umul128(lhs, rhs, &high);

			return low | -!!high;
		}

#endif

		// Signed
		template <typename T>
		constexpr auto mul(const T lhs, const T rhs) -> detail::return_if_signed_int_t<T>
		{
			using large_type = detail::next_size_t<T>;
			using u_type = std::make_unsigned_t<T>;

			// Compute the value to return in the event of overflow. This is either INT_MAX if the sign of the
			// inputs are the same or INT_MIN if the signs are opposite. As with signed addition, the signs
			// can be compared using a XOR check and the appropriate return can be computed by adding 1 or 0
			// to the max integer.
			//
			// Note: convert the result of the XOR to an unsigned type so that the right shift always zero-fills the left bits
			//
			const u_type overflow_result = std::numeric_limits<T>::max() + (static_cast<u_type>(lhs ^ rhs) >> detail::sign_bit_shift_v<T>);

			// As with the unsigned implementation, perform the multiplication on the next size larger type
			const large_type res = static_cast<large_type>(lhs) * static_cast<large_type>(rhs);

			// Separate the high and low bits
			const auto high = static_cast<T>(res >> detail::bit_size_v<T>);
			const auto low  = static_cast<T>(res);

			// Compute the sign-extended bits of the low bits. since low is signed the right shift will fill all the left
			// bits with whatever the most significant bit was, which gives the high bits of a sign extended number containing
			// the computed low bits.
			const T low_sign_ext = low >> detail::sign_bit_shift_v<T>;

			// Overflow has occured if the actual high bits of the operation do not match the high bits of the
			// sign-extended low bits (that is there numeric data in the high bits).
			return (high != low_sign_ext) ? overflow_result : low;
		}

#if CRN_SATMATH_MUL128()

		template <>
		int64_t mul<int64_t>(const int64_t lhs, const int64_t rhs)
		{
			using large_type = detail::next_size_t<T>;
			using u_type = std::make_unsigned_t<T>;

			// As with the unsigned implementation, perform the multiplication on the next size larger type.
			const large_type res = static_cast<large_type>(lhs) * static_cast<large_type>(rhs);

			// Compute the value to return in the event of overflow. This is either INT_MAX if the sign of the
			// inputs are the same or INT_MIN if the signs are opposite. As with signed addition, the signs
			// can be compared using a XOR check and the appropriate return can be computed by adding 1 or 0
			// to the max integer.
			//
			// Reminder: Convert the result of the XOR to an unsigned type so that the right shift always zero-fills the left bits
			//
			// Note: Putting this before or after the computation of res seems to matter for the assembly...putting this later seems
			//       to be better as it avoids instructions to get a relative address to anything.
			//
			const u_type overflow_result = std::numeric_limits<T>::max() + (static_cast<u_type>(lhs ^ rhs) >> detail::sign_bit_shift_v<T>);

			// Separate the high and low bits
			const auto high = static_cast<T>(res >> detail::bit_size_v<T>);
			const auto low = static_cast<T>(res);

			// Compute the sign-extended bits of the low bits. since low is signed the right shift will fill all the left
			// bits with whatever the most significant bit was, which gives the high bits of a sign extended number containing
			// the computed low bits.
			const T low_sign_ext = low >> detail::sign_bit_shift_v<T>;

			// Overflow has occured if the actual high bits of the operation do not match the high bits of the
			// sign-extended low bits (that is there numeric data in the high bits).
			return (high != low_sign_ext) ? overflow_result : low;
		}

#endif

		//
		// Division

		// Unsigned
		template <typename T>
		constexpr auto div(const T lhs, const T rhs) -> detail::return_if_unsigned_int_t<T>
		{
			// Neither overflow nor underflow are possible with unsigned integers
			return lhs / rhs;
		}

		// Signed
		template <typename T>
		constexpr auto div(const T lhs, const T rhs) -> detail::return_if_signed_int_t<T>
		{
			// Underflow is not possible with signed integers, but overflow is possible in 1 specific case: MIN / -1. Unlike the
			// other cases it is not sufficient to detect and deal with overflow because on x86 overflow as a result of division
			// causes a fault. Instead this case must be detected ahead of time and dealt with.
			//
			// The correct (saturated) result of this operation is the same as (MIN + 1) / -1, so in the problem case adding 1 to
			// the numerator (lhs) should be sufficient.
			//
			// Logic to detect the problem case:
			//   if ((rhs == -1) && (lhs == MIN))        { lhs += 1 }
			//   if ((rhs + 1 == 0) && (lhs - MIN == 0)) { lhs += 1 }
			//   if (!(rhs + 1) && !(lhs - MIN))         { lhs += 1 }
			//   if !((rhs + 1) || (lhs - MIN))          { lhs += 1 }
			//
			// Making it branchless:
			//   lhs += !((rhs + 1) | (lhs - MIN));
			//
			// Unfortunately, there is a chance that (rhs + 1) or (lhs - MIN) will overflow, which is not well defined for signed integers, so
			// one last change is required to convert this to unsigned arithmetic:
			//   lhs += !((urhs + 1) | (ulhs + uMIN));

			using u_type = std::make_unsigned_t<T>;

			const u_type ulhs = lhs;
			const u_type urhs = rhs;

			const T num = lhs + !((urhs + 1) | (ulhs + static_cast<u_type>(std::numeric_limits<T>::min())));
			return num / rhs;
		}
	}
}

#undef CRN_SATMATH_HAS_INT128
#undef CRN_SATMATH_MUL128