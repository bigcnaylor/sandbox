#pragma once

#include <iterator>
#include <type_traits>

namespace crn {

	// A type trait which exposes a type as...itself! This is useful for
	// opting function arguments out of template type deduction for example
	template <typename T>
	struct identity_type {
		using type = T;
	};

	template <typename T>
	using identity_type_t = identity_type<T>;

	// This will be in C++20
	template <typename T>
	struct remove_cvref {
		using type = std::remove_cv_t<std::remove_reference_t<T>>;
	};

	template <typename T>
	using remove_cvref_t = typename remove_cvref<T>::type;

	// A type trait to indicate if a type can be converted to another type by only
	// adding approprite qualifications (const or volatile) to the type
	template <typename From, typename To>
	struct is_qualification_convertible {
		static constexpr bool value = std::is_convertible_v<From, To>
			&& std::is_same_v<std::remove_cv_t<From>, std::remove_cv_t<To>>;
	};

	template <typename From, typename To>
	inline constexpr bool is_qualification_convertible_v = is_qualification_convertible<From, To>::value;

	// A type trait to determine if a given type has a specialization of std::iterator_traits with
	// the required type members
	template <typename T, typename = std::void_t<>>
	struct has_iterator_traits : public std::false_type {};

	template <typename T>
	struct has_iterator_traits<
		T,
		std::void_t<
			typename std::iterator_traits<T>::value_type,
			typename std::iterator_traits<T>::difference_type,
			typename std::iterator_traits<T>::reference,
			typename std::iterator_traits<T>::pointer,
			typename std::iterator_traits<T>::iterator_category
		>
	> : public std::true_type {};

	template <typename T>
	inline constexpr bool has_iterator_traits_v = has_iterator_traits<T>::value;

	// A type trait to determine if a given type may be dereferenced using the . (dot) operator
	template <typename T, typename = std::void_t<>>
	struct is_dot_dereferenceable : public std::false_type {};

	template <typename T>
	struct is_dot_dereferenceable <T, std::void_t<decltype(*std::declval<T>())>> : public std::true_type {};

	template <typename T>
	inline constexpr bool is_dot_dereferenceable_v = is_dot_dereferenceable<T>::value;

	// A type trait do determine if a given type may be dereferenced using the -> operator
	template <typename T, typename = std::void_t<>>
	struct is_arrow_dereferenceable : public std::false_type {};

	template <typename T>
	struct is_arrow_dereferenceable<T*, std::void_t<>> : public std::true_type {};

	template <typename T>
	struct is_arrow_dereferenceable<T, std::void_t<decltype(std::declval<T>().operator->())>> : public std::true_type {};

	template <typename T>
	inline constexpr bool is_arrow_dereferenceable_v = is_arrow_dereferenceable<T>::value;

	// A type trait to determine if a given type may be prefix incremented
	template <typename T, typename = std::void_t<>>
	struct is_prefix_incrementable : public std::false_type {};

	template <typename T>
	struct is_prefix_incrementable<T, std::void_t<decltype(++std::declval<T>())>> {
		static constexpr bool value = std::is_same_v<decltype(++std::declval<T>()), T&>;
	};

	template <typename T>
	struct is_prefix_incrementable<T&> : public is_prefix_incrementable<T> {};

	template <typename T>
	struct is_prefix_incrementable<T&&> : public is_prefix_incrementable<T> {};

	template <typename T>
	inline constexpr bool is_prefix_incrementable_v = is_prefix_incrementable<T>::value;

	// A type trait to determine if a given type may be postfix incremented
	template <typename T, typename = std::void_t<>>
	struct is_postfix_incrementable : public std::false_type {};

	template <typename T>
	struct is_postfix_incrementable<T, std::void_t<decltype(std::declval<T>()++)>> {
		static constexpr bool value = std::is_same_v<decltype(std::declval<T>()++), T>;
	};

	template <typename T>
	struct is_postfix_incrementable<T&> : public is_postfix_incrementable<T> {};

	template <typename T>
	struct is_postfix_incrementable<T&&> : public is_postfix_incrementable<T> {};

	template <typename T>
	inline constexpr bool is_postfix_incrementable_v = is_postfix_incrementable<T>::value;

	// A type trait to determine if a given type can be compared with the equality operator (==)
	template <typename T, typename = std::void_t<>>
	struct is_equality_comparable : public std::false_type {};

	template <typename T>
	struct is_equality_comparable<T, std::void_t<decltype(std::declval<T>() == std::declval<T>())>> {
		static constexpr bool value = std::is_convertible_v<decltype(std::declval<T>() == std::declval<T>()), bool>;
	};

	template <typename T>
	inline constexpr bool is_equality_comparable_v = is_equality_comparable<T>::value;

	// A type trait to determine if a given type can be compared with the inequality operator (!=)
	template <typename T, typename = std::void_t<>>
	struct is_inequality_comparable : public std::false_type {};

	template <typename T>
	struct is_inequality_comparable<T, std::void_t<decltype(std::declval<T>() != std::declval<T>())>> {
		static constexpr bool value = std::is_convertible_v<decltype(std::declval<T>() != std::declval<T>()), bool>;
	};

	template <typename T>
	inline constexpr bool is_inequality_comparable_v = is_inequality_comparable<T>::value;

	// A type trait to determine if a given type satisfies the requirements of a LegacyIterator
	template <typename T>
	struct is_legacy_iterator
	{
		static constexpr bool value = std::is_copy_constructible_v<T>
			&& std::is_copy_assignable_v<T>
			&& std::is_destructible_v<T>
			&& std::is_swappable_v<T>
			&& has_iterator_traits_v<T>
			&& is_dot_dereferenceable_v<T>
			&& is_prefix_incrementable_v<T>;
	};

	template <typename T>
	inline constexpr bool is_legacy_iterator_v = is_legacy_iterator<T>::value;

	// A type trait to determine if a given type satisfies the requirements of LegacyInputIterator
	template <typename T, typename = std::void_t<>>
	struct is_legacy_input_iterator : public std::false_type {};

	template <typename T>
	struct is_legacy_input_iterator<T, std::void_t<std::enable_if_t<is_legacy_iterator_v<T>>>> {
		static constexpr bool value = is_equality_comparable_v<T>
			&& is_inequality_comparable_v<T>
			&& is_postfix_incrementable_v<T>
			&& is_arrow_dereferenceable_v<T>
			&& std::is_convertible_v<decltype(*std::declval<T>()), std::iterator_traits<T>::value_type>;
	};

	template <typename T>
	inline constexpr bool is_legacy_input_iterator_v = is_legacy_input_iterator<T>::value;
}