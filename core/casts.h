#pragma once

namespace crn {

	template <typename To, typename From>
	constexpr To narrow_cast(From from)
	{
		const auto result = static_cast<To>(from);
		assert(result == from);
		return result;
	}
}