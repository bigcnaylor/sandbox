
#pragma warning(push)
#pragma warning(disable:4996) // std::result_of superceded by std::invoke_result in C++17

namespace crn {

	namespace variant_helper {
		using size_type = variant_size_type;

		// Value is true if the specified type is a variant
		template <typename T> struct is_variant : std::false_type {};
		template <typename... As> struct is_variant<variant<As...>> : std::true_type {};
		template <typename T> struct is_variant<const T> : is_variant<T> {};
		template <typename T> struct is_variant<volatile T> : is_variant<T> {};

		// Value is true if all specified types are variatns
		template <typename... Ts> struct all_are_variant;
		template <> struct all_are_variant<> : std::true_type {};

		template <typename T, typename... Ts>
		struct all_are_variant<T, Ts...> :
			std::integral_constant<bool, is_variant<T>::value && all_are_variant<Ts...>::value> {
		};

		// Value is true if the specified type is a variant alternative index tag
		template <typename T> struct is_variant_index_tag : std::false_type {};
		template <size_type I> struct is_variant_index_tag<variant_index_t<I>> : std::true_type {};

		// Value is true if the specified type is a variant alternative type tag
		template <typename T> struct is_variant_type_tag : std::false_type {};
		template <typename T> struct is_variant_type_tag<variant_type_t<T>> : std::true_type {};

		// Type trait provides value to indicate whether the specified type exists in the alternative parameter pack
		template <typename T, typename Alternative> struct alternative_exists<T, Alternative> : std::is_same<T, Alternative> {};

		template <typename T, typename Alternative, typename... As>
		struct alternative_exists<T, Alternative, As...> :
			std::integral_constant<bool, std::is_same<T, Alternative>::value || alternative_exists<T, As...>::value> {
		};

		// Type trait provides value to indicate whether the specified type exists in the alternative parameter
		// pack exactly once
		template <typename T, typename Alternative> struct alternative_exists_once<T, Alternative> : alternative_exists<T, Alternative> {};

		template <typename T, typename Alternative, typename... As>
		struct alternative_exists_once<T, Alternative, As...> {
		private:
			static constexpr bool is_alt  = alternative_exists<T, Alternative>::value;
			static constexpr bool in_rest = alternative_exists_once<T, As...>::value;

		public:
			static constexpr bool value = (is_alt && !in_rest) || (!is_alt && in_rest);
		};

		// Type trait provides a value to indicate the index of the specified type in the alternative parameter pack
		template <size_type starting_index, typename T, typename... As> struct alternative_type_index_impl;

		template <size_type starting_index, typename T, typename Alternative>
		struct alternative_type_index_impl<starting_index, T, Alternative> : std::integral_constant<size_type, starting_index> {
		};

		template <size_type starting_index, typename T, typename Alternative, typename... As>
		struct alternative_type_index_impl<starting_index, T, Alternative, As...> :
			std::integral_constant<size_type, std::is_same<T, Alternative>::value ? starting_index : alternative_type_index_impl<starting_index + 1, T, As...>::value> {
		};

		template <typename T, typename... As>
		struct alternative_type_index : alternative_type_index_impl<0, T, As...> {
			static_assert(alternative_exists_once<T, As...>::value, "Alternative must exist and only once in order to look up its index.");
		};

		// Type trait which provides the member constant value equal to true if all specified types are a valid
		// alternative for a variant.
		template <typename... As> struct all_types_valid;
		template <typename T>
		struct all_types_valid<T> :
			std::integral_constant<bool, !std::is_reference<T>::value && std::is_destructible<T>::value> {
		};

		template <typename T, typename... As>
		struct all_types_valid<T, As...> :
			std::integral_constant<bool, all_types_valid<T>::value && all_types_valid<As...>::value> {
		};

		// Type trait which provides the type alias for the alternative at the specified index in the parameter pack
		template <size_type I, size_type Start, typename... As> struct alternative_type_at_index_impl;

		template <size_type I, typename Alternative, typename... As>
		struct alternative_type_at_index_impl<I, I, Alternative, As...> {
			using type = Alternative;
		};

		template <size_type I, size_type Start, typename Alternative, typename... As>
		struct alternative_type_at_index_impl<I, Start, Alternative, As...> : alternative_type_at_index_impl<I, Start + 1, As...> {
		};

		template <size_type I, typename... As>
		struct alternative_type_at_index : alternative_type_at_index_impl<I, 0, As...> {
			static_assert(I < sizeof...(As), "Index out of range");
		};

		// Type trait which extracts the first type from a parameter pack along with its reference types
		template <typename... As>
		struct first_type : alternative_type_at_index<0, As...> {};

		// Type trait used to deduce the return type of a dispatch to a variant
		template <typename F, typename... As>
		struct dispatch_lvalue_result_deducer {
			using first_type = typename first_type<As...>::type;
			using arg_type   = typename std::add_lvalue_reference<first_type>::type;
			using type       = typename std::result_of<F(arg_type)>::type;
		};

		template <typename F, typename... As>
		struct dispatch_rvalue_result_deducer {
			using first_type = typename first_type<As...>::type;
			using arg_type   = typename std::add_rvalue_reference<first_type>::type;
			using type       = typename std::result_of<F(arg_type)>::type;
		};

		// Dispatch an lvalue to the provided function and return the specified type
		template <typename R, typename V, typename B, typename... As>
		struct dispatch_lvalue {
			using result_type = R;
			using visitor_type = typename std::remove_reference<V>::type;
			using block_type = B;

			using invoke_impl_type = result_type (visitor_type&, block_type&);

			template <typename T>
			static result_type invoke_impl_template(visitor_type& visitor, block_type& block)
			{
				return visitor(reinterpret_cast<T&>(block));
			}

			template <typename F>
			static result_type invoke(const variant_size_type index, block_type& block, F&& visitor)
			{
				static constexpr invoke_impl_type* invokers[] = {invoke_impl_template<As>...};
				return invokers[index](visitor, block);
			}
		};

		template <typename V, typename B, typename... As>
		struct dispatch_lvalue<void, V, B, As...> {

			// void specialization
			using visitor_type = typename std::remove_reference<V>::type;
			using block_type = B;

			using invoke_impl_type = void (visitor_type&, block_type&);

			template <typename T>
			static void invoke_impl_template(visitor_type& visitor, block_type& block)
			{
				visitor(reinterpret_cast<T&>(block));
			}

			template <typename F>
			static void invoke(const variant_size_type index, block_type& block, F&& visitor)
			{
				static constexpr invoke_impl_type* invokers[] = {invoke_impl_template<As>...};
				invokers[index](visitor, block);
			}
		};

		template <typename V, typename B, typename... As>
		struct dispatch_lvalue<const void, V, B, As...> : dispatch_lvalue<void, V, B, As...> {
		};

		template <typename V, typename B, typename... As>
		struct dispatch_lvalue<volatile void, V, B, As...> : dispatch_lvalue<void, V, B, As...> {
		};

		template <typename V, typename B, typename... As>
		struct dispatch_lvalue<const volatile void, V, B, As...> : dispatch_lvalue<void, V, B, As...> {
		};

		// Dispatch an lvalue to a function and deduce the returned type
		template <typename V, typename B, typename... As>
		struct dispatch_lvalue_deduced_result : dispatch_lvalue<typename dispatch_lvalue_result_deducer<V, As...>::type, V, B, As...> {
		};

		// Dispatch an rvalue to the provided function and return the specified type
		template <typename R, typename V, typename B, typename... As>
		struct dispatch_rvalue {
			using result_type = R;
			using visitor_type = typename std::remove_reference<V>::type;
			using block_type = B;

			using invoke_impl_type = result_type(visitor_type&, block_type&);

			template <typename T>
			static result_type invoke_impl_template(visitor_type& visitor, block_type& block)
			{
				auto& value = reinterpret_cast<T&>(block);
				return visitor(std::move(value));
			}

			template <typename F>
			static result_type invoke(const variant_size_type index, block_type& block, F&& visitor)
			{
				static constexpr invoke_impl_type* invokers[] = {invoke_impl_template<As>...};
				return invokers[index](visitor, block);
			}
		};

		template <typename V, typename B, typename... As>
		struct dispatch_rvalue<void, V, B, As...> {

			// void specialization
			using visitor_type = typename std::remove_reference<V>::type;
			using block_type = B;

			using invoke_impl_type = void(visitor_type&, block_type&);

			template <typename T>
			static void invoke_impl_template(visitor_type& visitor, block_type& block)
			{
				auto& value = reinterpret_cast<T&>(block);
				visitor(std::move(value));
			}

			template <typename F>
			static void invoke(const variant_size_type index, block_type& block, F&& visitor)
			{
				static constexpr invoke_impl_type* invokers[] = {invoke_impl_template<As>...};
				invokers[index](visitor, block);
			}
		};

		template <typename V, typename B, typename... As>
		struct dispatch_rvalue<const void, V, B, As...> : dispatch_rvalue<void, V, B, As...> {
		};

		template <typename V, typename B, typename... As>
		struct dispatch_rvalue<volatile void, V, B, As...> : dispatch_rvalue<void, V, B, As...> {
		};

		template <typename V, typename B, typename... As>
		struct dispatch_rvalue<const volatile void, V, B, As...> : dispatch_rvalue<void, V, B, As...> {
		};

		// Dispatch an rvalue to a function and deduce the returned type
		template <typename V, typename B, typename... As>
		struct dispatch_rvalue_deduced_result : dispatch_rvalue<typename dispatch_rvalue_result_deducer<V, As...>::type, V, B, As...> {
		};

		// Dispatch an lvalue to a function with a specified return type taking 2 variants which contain the same alternative
		template <typename R, typename V, typename B, typename... As>
		struct dispatch_binary_lvalue {
			using result_type = R;
			using visitor_type = typename std::remove_reference<V>::type;
			using block_type = B;

			using invoke_impl_type = result_type (visitor_type&, block_type&, block_type&);

			template <typename T>
			static R invoke_impl_template(visitor_type& visitor, block_type& block_1, block_type& block_2)
			{
				return visitor(reinterpret_cast<T&>(block_1), reinterpret_cast<T&>(block_2));
			}

			template <typename F>
			static R invoke(const variant_size_type index, block_type& block_1, block_type& block_2, F&& visitor)
			{
				static constexpr invoke_impl_type* invokers[] = {invoke_impl_template<As>...};
				return invokers[index](visitor, block_1, block_2);
			}
		};

		template <typename V, typename B, typename... As>
		struct dispatch_binary_lvalue<void, V, B, As...> {
			// void specialization
			using visitor_type = typename std::remove_reference<V>::type;
			using block_type = B;

			using invoke_impl_type = void (visitor_type&, block_type&, block_type&);

			template <typename T>
			static void invoke_impl_template(visitor_type& visitor, block_type& block_1, block_type& block_2)
			{
				visitor(reinterpret_cast<T&>(block_1), reinterpret_cast<T&>(block_2));
			}

			template <typename F>
			static void invoke(const variant_size_type index, block_type& block_1, block_type& block_2, F&& visitor)
			{
				static constexpr invoke_impl_type* invokers[] = {invoke_impl_template<As>...};
				invokers[index](visitor, block_1, block_2);
			}
		};

		template <typename V, typename B, typename... As>
		struct dispatch_binary_lvalue<const void, V, B, As...> : dispatch_binary_lvalue<void, V, B, As...> {
		};

		template <typename V, typename B, typename... As>
		struct dispatch_binary_lvalue<volatile void, V, B, As...> : dispatch_binary_lvalue<void, V, B, As...> {
		};

		template <typename V, typename B, typename... As>
		struct dispatch_binary_lvalue<const volatile void, V, B, As...> : dispatch_binary_lvalue<void, V, B, As...> {
		};

		// Call the provided function with the current alternative of each variant as its arguments. The return
		// type is deduced from the function.
		template <typename Visitor>
		decltype(auto) multi_variant_visit_impl(Visitor&& visitor)
		{
			return visitor();
		}

		template <typename Visitor, typename Variant>
		decltype(auto) multi_variant_visit_impl(Visitor&& visitor, Variant&& variant)
		{
			return std::forward<Variant>(variant).visit(std::forward<Visitor>(visitor));
		}

		template <typename Visitor, typename Variant, typename... Args>
		decltype(auto) multi_variant_visit_impl(Visitor&& visitor, Variant&& variant, Args&&... args)
		{
			return std::forward<Variant>(variant).visit(
				[&] (auto&& first_alt) {
					return multi_variant_visit_impl(
						[&] (auto&&... remaining_alts) {
							return std::forward<Visitor>(visitor)(
								std::forward<decltype(first_alt)>(first_alt),
								std::forward<decltype(remaining_alts)>(remaining_alts)...);
						},
						std::forward<Args>(args)...);
				});
		}

		// Call the provided function with the current alternative of each variant as its arguments. The return
		// type is explicitly specialized and the structure has been partially specialized to ignore the return
		// value if the type is void.
		//
		template <typename R>
		struct multi_variant_visit_with_return {

			template <typename Visitor, typename... Args>
			static R invoke(Visitor&& visitor, Args&&... args)
			{
				return multi_variant_visit_impl(std::forward<Visitor>(visitor), std::forward<Args>(args)...);
			}
		};

		template <>
		struct multi_variant_visit_with_return<void> {
			// Void specialization

			template <typename Visitor, typename... Args>
			static void invoke(Visitor&& visitor, Args&&... args)
			{
				// Ignore the return type
				multi_variant_visit_impl(
					[&visitor] (auto&&... as) -> void {
						std::forward<Visitor>(visitor)(std::forward<decltype(as)>(as)...);
					},
					std::forward<Args>(args)...);
			}
		};

		template <typename R>
		struct multi_variant_visit_with_return<const R> : multi_variant_visit_with_return<R> {
			// Strip const qualification from return type
		};

		template <typename R>
		struct multi_variant_visit_with_return<volatile R> : multi_variant_visit_with_return<R> {
			// Strip volatile qualification from return type
		};

		// Deducing the type for conversion construction and assignment is quite complicated. This struct should
		// handle all the heavy lifting to deduce the correct alternative type using C++14 compliant code
		template <typename T, typename... As>
		struct conversion_type_deducer {
			using deduced_type = typename std::remove_reference<T>::type;
			static constexpr bool deduced_type_is_alternative = alternative_exists<deduced_type, As...>::value;

			using no_cv_deduced_type = typename std::remove_cv<deduced_type>::type;
			static constexpr bool no_cv_deduced_type_is_alternative = alternative_exists<no_cv_deduced_type, As...>::value;

			static_assert(
				deduced_type_is_alternative || no_cv_deduced_type_is_alternative,
				"Cannot deduce the correct alternative to convert to from the provided type.");

			template <bool deduced_is_a>
			struct alternative_type_selector {
				using type = deduced_type;
			};

			template <>
			struct alternative_type_selector<false> {
				using type = no_cv_deduced_type;
			};

			using alternative_type = typename alternative_type_selector<deduced_type_is_alternative>::type;
			static_assert(
				alternative_exists_once<alternative_type, As...>::value,
				"Requested variant alternative is ambiguous. There are multiple alternatives of the deduced type so conversion is not possible.");

			using stored_type = typename std::remove_cv<alternative_type>::type;
			static constexpr size_type alternative_index = alternative_type_index<alternative_type, As...>::value;
		};

		// Functions which moves the source value into the destination buffer and destroys the source.
		//
		// returns: A pointer to the constructed instance.
		//
		template <typename T>
		typename std::remove_cv<T>::type* move_and_destroy(void* const dest, T& source)
		{
			using stored_type = typename std::remove_cv<T>::type;
			auto& source_storage = const_cast<stored_type&>(source);

			const auto result = new (dest) stored_type(std::move(source_storage));
			source_storage.~stored_type();

			return result;
		}

		// Functor types to perform copy and move operations on variants
		template <typename... As>
		class dispatched_operations {
		public:
			using variant_type = variant<As...>;
			using block_type = typename variant_type::block_type;
			using size_type = variant_size_type;

			class copy_construct_to {
			public:

				copy_construct_to(const variant_size_type index, variant_type& lhs) :
					m_lhs(&lhs),
					m_index(index)
				{
				}

				template <typename T>
				void operator() (const T& rhs_value) const
				{
					m_lhs->template construct_contents<T>(m_index, rhs_value);
				}

			private:
				variant_type* m_lhs;
				size_type m_index;
			};


			class move_construct_to {
			public:

				move_construct_to(const size_type index, variant_type& lhs) :
					m_lhs(&lhs),
					m_index(index)
				{
				}

				template <typename T>
				void operator() (T& rhs_value) const
				{
					m_lhs->template construct_contents<T>(m_index, std::move(rhs_value));
				}

			private:
				variant_type* m_lhs;
				size_type m_index;
			};

			class copy_assign_from {
			public:

				explicit copy_assign_from(const variant_type& rhs) :
					m_rhs(&rhs)
				{}

				template <typename T>
				void operator() (T& lhs_value)
				{
					using const_type = typename std::add_const<T>::type;

					auto& rhs_value = reinterpret_cast<const_type&>(m_rhs->m_block);
					lhs_value = rhs_value;
				}

			private:
				const variant_type* m_rhs;
			};

			class move_assign_from {
			public:

				explicit move_assign_from(variant_type& rhs) :
					m_rhs(&rhs)
				{
				}

				template <typename T>
				void operator() (T& lhs_value)
				{
					auto& rhs_value = reinterpret_cast<T&>(m_rhs->m_block);
					lhs_value = std::move(rhs_value);
				}

			private:
				variant_type* m_rhs;
			};

			class destroy {
			public:

				template <typename T>
				void operator() (T& value)
				{
					using stored_type = typename std::remove_cv<T>::type;
					auto& storage = const_cast<stored_type&>(value);
					storage.~stored_type();
				}
			};

			// Functor type to move the source value into the stored destination
			class destructive_move_to {
			public:

				template <typename T>
				explicit destructive_move_to(T& dest) :
					m_dest(&dest)
				{
				}

				template <typename T>
				void operator() (T& value)
				{
					move_and_destroy(m_dest, value);
				}

			private:
				void* m_dest;
			};

			class destructive_swap {
			public:

				destructive_swap(variant_type& rhs) :
					m_rhs(&rhs)
				{
				}

				template <typename T>
				void operator() (T& lhs_value) const
				{
					using stored_lhs_type = typename std::remove_cv<T>::type;
					auto& lhs_stored = const_cast<stored_lhs_type&>(lhs_value);

					// First, move the value into a temp buffer and destroy the source
					block_type temp_block;
					stored_lhs_type& temp_stored = *move_and_destroy(&temp_block, lhs_stored);

					// Second, move the rhs value into the lhs memory and destroy the rhs value
					m_rhs->visit(destructive_move_to{lhs_stored});

					// Finally, move the temp value into the rhs memory and destroy the temp value
					move_and_destroy(&m_rhs->m_block, temp_stored);
				}

			private:
				variant_type* m_rhs;
			};
		};

		// Value is true if the specified type is a valid argument to the conversion constructor
		template <typename T, typename... As>
		struct unambiguous_convert_construct {

			// The standard conversion constructor is more strict than this, but it's like...really complicated.
			// Fitting with the "minimum viable" vision of this undertaking I didn't want to use SFINAE at all, but
			// I think it is necessary to avoid ambiguity between conversion, emplacement, copy, and move construction.
			// Whether or not conversions are valid will be left up to other checked and compiler errors to catch.
			using naked_type   = remove_cvref_t<T>;
			using variant_type = variant<As...>;

			static constexpr bool value = !is_variant_index_tag<naked_type>::value
				&& !is_variant_type_tag<naked_type>::value
				&& !std::is_same<naked_type, variant_type>::value;
		};

		// Value is ture if the specified type is a valid right-hand value for conversion assignment
		template <typename T, typename... As>
		struct unambiguous_convert_assign {

			// Prevent ambiguity with the copy/move assignment operators of the variant itself
			using naked_type = remove_cvref_t<T>;
			using variant_type = variant<As...>;

			static constexpr bool value = !std::is_same<naked_type, variant_type>::value;
		};
	}
}

//
// variant_size trait implementation

template <typename... T>
struct crn::variant_size<crn::variant<T...>> :
	std::integral_constant<variant_size_type, static_cast<variant_size_type>(sizeof...(T))> {
};

template <typename T>
struct crn::variant_size<const T> : variant_size<T> {};

template <typename T>
struct crn::variant_size<volatile T> : variant_size<T> {};

template <typename T>
struct crn::variant_size<const volatile T> : variant_size<T> {};

//
// variant_alternative trait implementation

template <crn::variant_size_type I, typename... T>
struct crn::variant_alternative<I, crn::variant<T...>> {
	using type = typename variant_helper::alternative_type_at_index<I, T...>::type;
};

template <crn::variant_size_type I, typename T>
struct crn::variant_alternative<I, const T> {
	using type = typename std::add_const<variant_alternative<I, T>>::type;
};

template <crn::variant_size_type I, typename T>
struct crn::variant_alternative<I, volatile T> {
	using type = typename std::add_volatile<variant_alternative<I, T>>::type;
};

template <crn::variant_size_type I, typename T>
struct crn::variant_alternative<I, const volatile T> {
	using type = typename std::add_cv<variant_alternative<I, T>>::type;
};

//
// Implementation

template <typename... As>
crn::variant<As...>::variant() :
	// default-construct the first alternative
	variant(variant_index<0>)
{
}

template <typename... As>
crn::variant<As...>::variant(const variant& other)
{
	this->copy_construct_contents(other);
}

template <typename... As>
crn::variant<As...>::variant(variant&& other)
{
	this->move_construct_contents(other);
}

template <typename... As>
template <typename T, typename>
crn::variant<As...>::variant(T&& other)
{
	this->convert_construct_contents<T>(std::forward<T>(other));
}

template <typename... As>
template <typename T, typename... Args>
crn::variant<As...>::variant(variant_type_t<T>, Args&&... args)
{
	static_assert(
		alternative_exists_once<T>,
		"Requested type is not a valid alternative. The constructed type must be one of the types "
		"specified in the variant and that type must appear in the list of alternatives only once.");

	static constexpr size_type type_index = alternative_type_index<T>;
	this->construct_contents<T>(type_index, std::forward<Args>(args)...);
}

template <typename... As>
template <crn::variant_size_type I, typename... Args>
crn::variant<As...>::variant(variant_index_t<I>, Args&&... args)
{
	static_assert(I < alternative_count, "Index out of range.");

	using create_type = alternative_type_at_index<I>;
	this->construct_contents<create_type>(I, std::forward<Args>(args)...);
}

template <typename... As>
crn::variant<As...>::~variant()
{
	this->destroy_contents();
}

template <typename... As>
auto crn::variant<As...>::operator= (const variant& other) -> variant&
{
	this->copy_assign_contents(other);
	return *this;
}

template <typename... As>
auto crn::variant<As...>::operator= (variant&& other) -> variant&
{
	this->move_assign_contents(other);
	return *this;
}

template <typename... As>
template <typename T, typename>
auto crn::variant<As...>::operator= (T&& other) ->variant&
{
	this->convert_assign_contents<T>(std::forward<T>(other));
	return *this;
}

template <typename... As>
auto crn::variant<As...>::index() const -> size_type
{
	return this->m_index;
}

template <typename... As>
template <typename T>
bool crn::variant<As...>::holds_alternative() const
{
	static_assert(
		alternative_exists_once<T>,
		"Requested variant alternative is ambiguous or doesn't exist.");

	static constexpr variant_size_type type_index = alternative_type_index<T>;
	return this->m_index == type_index;
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get() & -> alternative_t<I>&
{
	if (this->m_index != I) {
		CRN_FATAL_ERROR("Variant does not contain requested alternative.");
	}

	return this->get_unchecked<I>();
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get() const& -> const alternative_t<I>&
{
	if (this->m_index != I) {
		CRN_FATAL_ERROR("Variant does not contain requested alternative.");
	}

	return this->get_unchecked<I>();
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get() && -> alternative_t<I>&&
{
	auto& result = this->get<I>();
	return std::move(result);
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get() const&& -> const alternative_t<I>&&
{
	auto& result = this->get<I>();
	return std::move(result);
}

template <typename... As>
template <typename T>
T& crn::variant<As...>::get() &
{
	static_assert(
		alternative_exists_once<T>,
		"Requested type is not a valid alternative. The requested type must be one of the types "
		"specified in the variant and that type must appear in the list of alternatives only once.");

	static constexpr size_type alternative_index = alternative_type_index<T>;

	if (this->m_index != alternative_index) {
		CRN_FATAL_ERROR("Variant does not contain requested alternative.");
	}

	return this->get_unchecked<T>();
}

template <typename... As>
template <typename T>
const T& crn::variant<As...>::get() const&
{
	static_assert(
		alternative_exists_once<T>,
		"Requested type is not a valid alternative. The requested type must be one of the types "
		"specified in the variant and that type must appear in the list of alternatives only once.");

	static constexpr size_type alternative_index = alternative_type_index<T>;

	if (this->m_index != alternative_index) {
		CRN_FATAL_ERROR("Variant does not contain requested alternative.");
	}

	return this->get_unchecked<T>();
}

template <typename... As>
template <typename T>
T&& crn::variant<As...>::get() &&
{
	auto& result = this->get<T>();
	return std::move(result);
}

template <typename... As>
template <typename T>
const T&& crn::variant<As...>::get() const&&
{
	auto& result = this->get<T>();
	return std::move(result);
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get_unchecked() & -> alternative_t<I>&
{
	CRN_ASSERT(m_index == I, "Retrieving the wrong alternative (unchecked).");
	using result_type = alternative_t<I>;

	return reinterpret_cast<result_type&>(this->m_block);
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get_unchecked() const& -> const alternative_t<I>&
{
	CRN_ASSERT(m_index == I, "Retrieving the wrong alternative (unchecked).");
	using result_type = typename std::add_const<alternative_t<I>>::type;

	return reinterpret_cast<result_type&>(this->m_block);
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get_unchecked() && -> alternative_t<I>&&
{
	auto& result = this->get_unchecked<I>();
	return std::move(result);
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get_unchecked() const&& -> const alternative_t<I>&&
{
	auto& result = this->get_unchecked<I>();
	return std::move(result);
}

template <typename... As>
template <typename T>
T& crn::variant<As...>::get_unchecked() &
{
	CRN_ASSERT(m_index == alternative_type_index<T>, "Retrieving the wrong alternative (unchecked).");
	return reinterpret_cast<T&>(this->m_block);
}

template <typename... As>
template <typename T>
const T& crn::variant<As...>::get_unchecked() const&
{
	CRN_ASSERT(m_index == alternative_type_index<T>, "Retrieving the wrong alternative (unchecked).");
	return reinterpret_cast<const T&>(this->m_block);
}

template <typename... As>
template <typename T>
T&& crn::variant<As...>::get_unchecked() &&
{
	auto& result = this->get_unchecked<T>();
	return std::move(result);
}

template <typename... As>
template <typename T>
const T&& crn::variant<As...>::get_unchecked() const&&
{
	auto& result = this->get_unchecked<T>();
	return std::move(result);
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get_if() -> alternative_t<I>*
{
	return this->m_index == I ? &(this->get_unchecked<I>()) : nullptr;
}

template <typename... As>
template <crn::variant_size_type I>
auto crn::variant<As...>::get_if() const -> const alternative_t<I>*
{
	return this->m_index == I ? &(this->get_unchecked<I>()) : nullptr;
}

template <typename... As>
template <typename T>
auto crn::variant<As...>::get_if() -> T*
{
	static_assert(
		alternative_exists_once<T>,
		"Requested type is not a valid alternative. The requested type must be one of the types "
		"specified in the variant and that type must appear in the list of alternatives only once.");

	static constexpr size_type alternative_index = alternative_type_index<T>;
	return this->m_index == alternative_index ? &(this->get_unchecked<T>()) : nullptr;
}

template <typename... As>
template <typename T>
auto crn::variant<As...>::get_if() const -> const T*
{
	static_assert(
		alternative_exists_once<T>,
		"Requested type is not a valid alternative. The requested type must be one of the types "
		"specified in the variant and that type must appear in the list of alternatives only once.");

	static constexpr size_type alternative_index = alternative_type_index<T>;
	return this->m_index == alternative_index ? &(this->get_unchecked<T>()) : nullptr;
}

template <typename... As>
template <typename V>
decltype(auto) crn::variant<As...>::visit(V&& visitor) &
{
	using dispatcher = variant_helper::dispatch_lvalue_deduced_result<V, block_type, As...>;
	return dispatcher::invoke(this->m_index, this->m_block, std::forward<V>(visitor));
}

template <typename... As>
template <typename R, typename V>
R crn::variant<As...>::visit(V&& visitor) &
{
	using dispatcher = variant_helper::dispatch_lvalue<R, V, block_type, As...>;
	return dispatcher::invoke(this->m_index, this->m_block, std::forward<V>(visitor));
}

template <typename... As>
template <typename V>
decltype(auto) crn::variant<As...>::visit(V&& visitor) const&
{
	using dispatcher = variant_helper::dispatch_lvalue_deduced_result<V, const block_type, typename std::add_const<As>::type...>;
	return dispatcher::invoke(this->m_index, this->m_block, std::forward<V>(visitor));
}

template <typename... As>
template <typename R, typename V>
R crn::variant<As...>::visit(V&& visitor) const&
{
	using dispatcher = variant_helper::dispatch_lvalue<R, V, const block_type, typename std::add_const<As>::type...>;
	return dispatcher::invoke(this->m_index, this->m_block, std::forward<V>(visitor));
}

template <typename... As>
template <typename V>
decltype(auto) crn::variant<As...>::visit(V&& visitor) &&
{
	using dispatcher = variant_helper::dispatch_rvalue_deduced_result<V, block_type, As...>;
	return dispatcher::invoke(this->m_index, this->m_block, std::forward<V>(visitor));
}

template <typename... As>
template <typename R, typename V>
R crn::variant<As...>::visit(V&& visitor) &&
{
	using dispatcher = variant_helper::dispatch_rvalue<R, V, block_type, As...>;
	return dispatcher::invoke(this->m_index, this->m_block, std::forward<V>(visitor));
}

template <typename... As>
template <typename V>
decltype(auto) crn::variant<As...>::visit(V&& visitor) const&&
{
	using dispatcher = variant_helper::dispatch_rvalue_deduced_result<V, const block_type, typename std::add_const<As>::type...>;
	return dispatcher::invoke(this->m_index, this->m_block, std::forward<V>(visitor));
}

template <typename... As>
template <typename R, typename V>
R crn::variant<As...>::visit(V&& visitor) const&&
{
	using dispatcher = variant_helper::dispatch_rvalue<R, V, const block_type, typename std::add_const<As>::type...>;
	return dispatcher::invoke(this->m_index, this->m_block, std::forward<V>(visitor));
}

template <typename... As>
template <typename T, typename... Args>
T& crn::variant<As...>::emplace(Args&&... args)
{
	static_assert(
		alternative_exists_once<T>,
		"Requested variant alternative is ambiguous or doesn't exists.");

	static constexpr size_type type_index = alternative_type_index<T>;

	this->destroy_contents();
	return this->construct_contents<T>(type_index, std::forward<Args>(args)...);
}

template <typename... As>
template <crn::variant_size_type I, typename... Args>
auto crn::variant<As...>::emplace(Args&&... args) -> alternative_t<I>&
{
	static_assert(I >= 0 && I < alternative_count, "Index out of range.");
	using alternative_type = alternative_t<I>;

	this->destroy_contents();
	return this->construct_contents<alternative_type>(I, std::forward<Args>(args)...);
}

template <typename... As>
void crn::variant<As...>::swap(variant& rhs)
{
	using std::swap;

	variant& lhs = *this;

	if (lhs.m_index == rhs.m_index) {

		// Variants hold the same alternative, swap their values
		dispatch_binary<void>(rhs.m_index, lhs.m_block, rhs.m_block, [] (auto& l, auto& r) { swap(l, r); });

	} else {

		// Swap the contents
		using swapper = typename dispatched_operations::destructive_swap;
		using lhs_dispatcher = variant_helper::dispatch_lvalue<void, swapper, block_type, As...>;
		lhs_dispatcher::invoke(lhs.m_index, lhs.m_block, swapper{rhs});

		// And the index
		swap(lhs.m_index, rhs.m_index);
	}
}

template <typename... As>
bool crn::variant<As...>::operator==(const variant& rhs) const
{
	auto& lhs = *this;
	return (lhs.m_index == rhs.m_index)
		&& dispatch_binary<bool>(lhs.m_index, lhs.m_block, rhs.m_block, [] (const auto& l, const auto& r) { return l == r; });
}

template <typename... As>
bool crn::variant<As...>::operator!=(const variant& rhs) const
{
	auto& lhs = *this;
	return (lhs.m_index != rhs.m_index)
		|| dispatch_binary<bool>(lhs.m_index, lhs.m_block, rhs.m_block, [] (const auto& l, const auto& r) { return l != r; });
}

template <typename... As>
bool crn::variant<As...>::operator<(const variant& rhs) const
{
	auto& lhs = *this;
	if (lhs.m_index < rhs.m_index) {
		return true;
	}

	if (rhs.m_index < lhs.m_index) {
		return false;
	}

	return dispatch_binary<bool>(lhs.m_index, lhs.m_block, rhs.m_block, [] (const auto& l, const auto& r) { return l < r; });
}

template <typename... As>
bool crn::variant<As...>::operator>(const variant& rhs) const
{
	auto& lhs = *this;
	if (lhs.m_index > rhs.m_index) {
		return true;
	}

	if (rhs.m_index > lhs.m_index) {
		return false;
	}

	return dispatch_binary<bool>(lhs.m_index, lhs.m_block, rhs.m_block, [] (const auto& l, const auto& r) { return l > r; });
}

template <typename... As>
bool crn::variant<As...>::operator<=(const variant& rhs) const
{
	auto& lhs = *this;
	if (lhs.m_index < rhs.m_index) {
		return true;
	}

	if (rhs.m_index < lhs.m_index) {
		return false;
	}

	return dispatch_binary<bool>(lhs.m_index, lhs.m_block, rhs.m_block, [] (const auto& l, const auto& r) { return l <= r; });
}

template <typename... As>
bool crn::variant<As...>::operator>=(const variant& rhs) const
{
	auto& lhs = *this;
	if (lhs.m_index > rhs.m_index) {
		return true;
	}

	if (rhs.m_index > lhs.m_index) {
		return false;
	}

	return dispatch_binary<bool>(lhs.m_index, lhs.m_block, rhs.m_block, [] (const auto& l, const auto& r) { return l >= r; });
}

template <typename... As>
template <typename R, typename V>
R crn::variant<As...>::dispatch_binary(const size_type type_index, block_type& block_1, block_type& block_2, V&& visitor)
{
	using dispatcher = variant_helper::dispatch_binary_lvalue<R, V, block_type, As...>;
	return dispatcher::invoke(type_index, block_1, block_2, std::forward<V>(visitor));
}

template <typename... As>
template <typename R, typename V>
R crn::variant<As...>::dispatch_binary(const size_type type_index, const block_type& block_1, const block_type& block_2, V&& visitor)
{
	using dispatcher = variant_helper::dispatch_binary_lvalue<R, V, const block_type, typename std::add_const<As>::type...>;
	return dispatcher::invoke(type_index, block_1, block_2, std::forward<V>(visitor));
}

template <typename... As>
template <typename T, typename... Args>
T& crn::variant<As...>::construct_contents(const size_type index, Args&&... args)
{
	using stored_type = typename std::remove_cv<T>::type;

	const auto result = new (&this->m_block) stored_type(std::forward<Args>(args)...);
	this->m_index = index;

	return *result;
}

template <typename... As>
void crn::variant<As...>::copy_construct_contents(const variant& other)
{
	using do_copy = typename dispatched_operations::copy_construct_to;
	other.visit<void>(do_copy{other.m_index, *this});
}

template <typename... As>
void crn::variant<As...>::copy_assign_contents(const variant& other)
{
	if (this->m_index != other.m_index) {

		this->destroy_contents();
		this->copy_construct_contents(other);
	} else {

		// The types are the same. Invoke the lhs alternative's assignment operator
		using do_assign = typename dispatched_operations::copy_assign_from;
		this->visit(do_assign{other});
	}
}

template <typename... As>
void crn::variant<As...>::move_construct_contents(variant& other)
{
	using do_move = typename dispatched_operations::move_construct_to;
	other.visit(do_move{other.m_index, *this});
}

template <typename... As>
void crn::variant<As...>::move_assign_contents(variant& other)
{
	if (this->m_index != other.m_index) {

		this->destroy_contents();
		this->move_construct_contents(other);
	} else {

		// The types are the same. Invoke the lhs alternative's assignment operator
		using do_assign = typename dispatched_operations::move_assign_from;
		this->visit(do_assign{other});
	}
}

template <typename... As>
template <typename T>
void crn::variant<As...>::convert_construct_contents(typename std::remove_reference<T>::type& lvalue_reference)
{
	using deducer = variant_helper::conversion_type_deducer<T, As...>;
	construct_contents<typename deducer::stored_type>(deducer::alternative_index, lvalue_reference);
}

template <typename... As>
template <typename T>
void crn::variant<As...>::convert_assign_contents(typename std::remove_reference<T>::type& lvalue_reference)
{
	using deducer = variant_helper::conversion_type_deducer<T, As...>;

	if (this->m_index != deducer::alternative_index) {

		// The variant contains a different alternative. Destroy it and construct the deduced alternative.
		this->destroy_contents();
		this->construct_contents<typename deducer::stored_type>(deducer::alternative_index, lvalue_reference);
	} else {

		// The variant already holds the deduced alternative, call the alternative's assignment operator.
		auto& value = reinterpret_cast<typename deducer::alternative_type&>(this->m_block);
		value = lvalue_reference;
	}
}

template <typename... As>
template <typename T>
void crn::variant<As...>::convert_construct_contents(typename std::remove_reference<T>::type&& rvalue_reference)
{
	using deducer = variant_helper::conversion_type_deducer<T, As...>;
	construct_contents<typename deducer::stored_type>(deducer::alternative_index, std::move(rvalue_reference));
}

template <typename... As>
template <typename T>
void crn::variant<As...>::convert_assign_contents(typename std::remove_reference<T>::type&& rvalue_reference)
{
	using deducer = variant_helper::conversion_type_deducer<T, As...>;

	if (this->m_index != deducer::alternative_index) {

		// The variant contains a different alternative. Destroy it and construct the deduced alternative.
		this->destroy_contents();
		this->construct_contents<typename deducer::stored_type>(deducer::alternative_index, std::move(rvalue_reference));
	} else {

		// The variant already holds the deduced alternative, call the alternative's assignment operator.
		auto& value = reinterpret_cast<typename deducer::alternative_type&>(this->m_block);
		value = std::move(rvalue_reference);
	}
}

template <typename... As>
void crn::variant<As...>::destroy_contents()
{
	using do_destroy = typename dispatched_operations::destroy;
	this->visit(do_destroy{});
}

// Specialize the get free function
template <crn::variant_size_type I, typename... As>
crn::variant_alternative_t<I, crn::variant<As...>>& crn::get(crn::variant<As...>& v)
{
	return v.template get<I>();
}

template <crn::variant_size_type I, typename... As>
const crn::variant_alternative_t<I, crn::variant<As...>>& crn::get(const crn::variant<As...>& v)
{
	return v.template get<I>();
}

template <crn::variant_size_type I, typename... As>
crn::variant_alternative_t<I, crn::variant<As...>>&& crn::get(crn::variant<As...>&& v)
{
	return std::move(v).template get<I>();
}

template <crn::variant_size_type I, typename... As>
const crn::variant_alternative_t<I, crn::variant<As...>>&& crn::get(const crn::variant<As...>&& v)
{
	return std::move(v).template get<I>();
}

template <typename T, typename... As>
T& crn::get(crn::variant<As...>& v)
{
	return v.template get<T>();
}

template <typename T, typename... As>
const T& crn::get(const crn::variant<As...>& v)
{
	return v.template get<T>();
}

template <typename T, typename... As>
T&& crn::get(crn::variant<As...>&& v)
{
	return std::move(v).template get<T>();
}

template <typename T, typename... As>
const T&& crn::get(const crn::variant<As...>&& v)
{
	return std::move(v).template get<T>();
}

// Specialize the get_if free function
template <crn::variant_size_type I, typename... As>
typename std::add_pointer<crn::variant_alternative_t<I, crn::variant<As...>>>::type crn::get_if(crn::variant<As...>& v)
{
	return v.template get_if<I>();
}

template <crn::variant_size_type I, typename... As>
typename std::add_pointer<const crn::variant_alternative_t<I, crn::variant<As...>>>::type crn::get_if(const crn::variant<As...>& v)
{
	return v.template get_if<I>();
}

template <typename T, typename... As>
typename std::add_pointer<T>::type crn::get_if(crn::variant<As...>& v)
{
	return v.template get_if<T>();
}

template <typename T, typename... As>
typename std::add_pointer<const T>::type crn::get_if(const crn::variant<As...>& v)
{
	return v.template get_if<T>();
}

// Invoke a single function with the current alternative of multiple variants as the arguments
template <typename Visitor, typename... Args>
decltype(auto) crn::visit(Visitor&& visitor, Args&&... args)
{
	static_assert(
		variant_helper::all_are_variant<typename std::remove_reference<Args>::type...>::value,
		"All visited argument types must be variants. At least 1 is not.");

	return variant_helper::multi_variant_visit_impl(std::forward<Visitor>(visitor), std::forward<Args>(args)...);
}

template <typename R, typename Visitor, typename... Args>
R crn::visit(Visitor&& visitor, Args&&... args)
{
	static_assert(
		variant_helper::all_are_variant<typename std::remove_reference<Args>::type...>::value,
		"All visited argument types must be variants. At least 1 is not.");

	using dispatcher = variant_helper::multi_variant_visit_with_return<R>;
	return dispatcher::invoke(std::forward<Visitor>(visitor), std::forward<Args>(args)...);
}

// Swap specialization
template <typename... As>
void crn::swap(crn::variant<As...>& lhs, crn::variant<As...>& rhs)
{
	lhs.swap(rhs);
}

#pragma warning(pop)