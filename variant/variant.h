#pragma once

#if !defined(CRN_VARIANT_H)
#define CRN_VARIANT_H

#include <cstdint>
#include <functional>
#include <type_traits>
#include <utility>

#include "../core/error.h"
#include "../core/type_traits.h"
#include "monostate.h"

namespace crn {

	// A variant is a type-safe union, which always holds exactly 1 value of one of its alternative types. This implementation
	// is modelled after the std::variant introduced in C++17 (https://en.cppreference.com/w/cpp/utility/variant), but is
	// designed to work in an environment that does not allow exceptions to be thrown, needs to be as efficient as practical
	// with memory, and may need to build with compilers that do not support C++17 (minimum C++14 likely required). Several
	// features or gaurantees made by std::variant were relaxed in the interest in simplicity.
	//
	// Each variant must specify 1 or more alternative types. These types may be const or volatile qualified, and the variant
	// may specify multiple alternatives of the same type. However, the variant cannot hold alternatives of reference, array,
	// or void types. The closest approximation to a variant with no alternatives (because everything eventually happens in
	// generic programming) is a variant with "monostate" as the only alternative.
	//
	// Like all unions, the memory required by the stored value is contained within the variant object; variant performs no
	// dynamic allocation. Also, like a union, variant must contain a value of 1 of its alternative types at any given time.
	// An "empty" variant can be modeled by using "monostate" as one of the alternatives. The default constructor will call
	// the default constructor of the first alternative in the type list; if this type is not default constructible any
	// attempt to call the default constructor on the variant will result in a compiler error.
	//
	// Additional constraints not present on std::variant:
	//   * May not throw exceptions
	//   * Must minimize memory usage
	//   * Support for pre C++17 compilers (improvements possible if this constraint is lifted)
	//   * It must be possible to match performance of a specialized type-safe/tagged union
	//
	// Modifications to support lack of exceptions:
	//   * Do not permit valueless_by_exception
	//   * Ommit noexcept since nothing throws
	//   * Halt execution (FATAL_ERROR) in place of throwing bad_variant_access
	//
	// Modifications to minimize memory usage:
	//   * Type index is stored as a single byte. 256 alternatives is enough for anyone, right?
	//
	// Modifications for pre-C++17 compilers:
	//   * Use long-form of type traits (no _v or _t unless it appeared in C++14)
	//   * Manually implement type traits not present prior to C++17 when required
	//   * Manually implement variant tag types and "monostate", even though the standard ones would work
	//   * Use std::result_of instead of std::invoke_result when deducing return type
	//   * Call visitor functions directly instead of using std::invoke
	//
	// Modifications to support performance optimization:
	//   * get_unchecked member functions allow the extraction of alternative values assuming the caller
	//     has already validated the held alternative. As the name suggests, it does not (in release builds) verify the
	//     requested alternative is held. Can be used to implement switch on type instead of relying on "visit".
	//
	// Simplifications:
	//   * No SFINAE, static_asserts and compiler errors used to enforce permitted operations instead. With 2 exceptions noted below.
	//   * No support for constexpr
	//   * No support for optionally trivial constructors, destructors, or assignment
	//   * No constructor or emplace overloads for std::initializer_list
	//   * No specialization for the std::hash algorithm
	//
	// Additions:
	//   * get functions available as members instead of only as free functions (though the latter are still supported)
	//   * visit function available as a member function for visiting a single variant.
	//
	// Exceptions to "No SFINAE":
	//   * Conversion constructor uses SFINAE to avoid ambiguity with copy, move, and emplacement constructors
	//   * Conversion assignment operator uses SFINAE to avoid ambiguuity with copy and move assignment

	//
	// Helper Types

	// The unsigned integer type used to track variant alternative indices
	using variant_size_type = uint8_t;

	// Type trait to provide a nested value which is the number of alternatives in a variant at compile time
	template <typename T> struct variant_size;

	template <typename T>
	constexpr variant_size_type variant_size_v = variant_size<T>::value;

	// Type trait to provide a nested type which is the type at the specified index in the specified variant's
	// list of alternatives
	template <variant_size_type I, typename T> struct variant_alternative;

	template <variant_size_type I, typename T>
	using variant_alternative_t = typename variant_alternative<I, T>::type;

	// Tag type to indicate which alternative to construct in a variant by index
	template <variant_size_type I> struct variant_index_t {
		explicit variant_index_t() = default;
	};

	template <variant_size_type I>
	constexpr variant_index_t<I> variant_index{};

	// Tag type to indicate which alternative to construct in a variant by type. This can only be used
	// if the specified type exists in the variant's list of alternatives exactly once.
	template <typename T> struct variant_type_t {
		explicit variant_type_t() = default;
	};

	template <typename T>
	static constexpr variant_type_t<T> variant_type{};

	//
	// Inernal Stuff (ignore me, but needs to be declared before the variant itself)

	// Forward-declare some traits and types to help implement the variant
	namespace variant_helper {

		// Value is true if all types in the parameter pack are valid alternatives for a variant
		template <typename... As> struct all_types_valid;

		// Value is true if the specified type exists in the parameter pack at all
		template <typename T, typename... As> struct alternative_exists;

		// Value is true if the specified type exists in the parameter pack exactly once
		template <typename T, typename... As> struct alternative_exists_once;

		// Value is the index at which the specified type exists in the paramter pack
		template <typename T, typename... As> struct alternative_type_index;

		// Type is the type in the parameter pack at the specified index
		template <variant_size_type I, typename... As> struct alternative_type_at_index;

		// Internal implementation
		template <typename... As> class dispatched_operations;

		// Value is true if the specified type is a valid argument to the conversion constructor
		template <typename T, typename... As> struct unambiguous_convert_construct;
		template <typename T, typename... As> using enable_convert_construct_t = std::enable_if_t<unambiguous_convert_construct<T, As...>::value>;

		// Value is ture if the specified type is a valid right-hand value for conversion assignment
		template <typename T, typename... As> struct unambiguous_convert_assign;
		template <typename T, typename... As> using enable_convert_assign_t = std::enable_if_t<unambiguous_convert_assign<T, As...>::value>;
	}

	// A generic typesafe tagged union (similar to std::variant, but simpler). See the big comment above.
	//
	template <typename... Alternatives>
	class variant {
	public:
		static_assert(sizeof...(Alternatives) > 0, "All variants must hold at least 1 alternative.");
		static_assert(variant_helper::all_types_valid<Alternatives...>::value, "Variants may not contain references or arrays.");

		using size_type = variant_size_type;

		template <size_type I>
		using alternative_t = variant_alternative_t<I, variant>;

		//
		// Constructions

		// Default
		variant();

		// Copy / move
		variant(const variant& other);
		variant(variant&& other);

		// Conversion
		//
		// If the type of "other" is an unambiguous reference to one of the variant's alternative types, the variant
		// is constructed with a copy or move (based on reference type) of the value. If it is not an unambiguous
		// alternative, is the variant type, or is one of the alternative tag types this overload is deleted.
		//
		template <typename T, typename = variant_helper::enable_convert_construct_t<T, Alternatives...>>
		explicit variant(T&& other);

		// Emplacement
		//
		// Construct the specified alternative by forwarding the provided arguments on to the alternative's constructor.
		// If the type is specified, it must refer to a type that appears in the list of alternatives exactly once.
		//
		template <typename T, typename... Args>
		explicit variant(variant_type_t<T>, Args&&... args);

		template <size_type I, typename... Args>
		explicit variant(variant_index_t<I>, Args&&... args);

		// Destructor
		~variant();

		//
		// Assignment

		// Copy / move
		//
		// If both variants hold the same alternative, the alternative will be copied or moved. If the variants
		// contain different alternatives, the alternative on the left is destroyed and a new alternative of the
		// right-hand type is constructed as a copy or move.
		//
		variant& operator= (const variant& other);
		variant& operator= (variant&& other);

		// Conversion
		//
		// If the type of "other" is and unambiguous reference to one of the alternative types and
		// that is the alternative held by the variant, the alternative is assigned to the value. If
		// the type is an unambiguous alternative and the variant contains a different alternative, the
		// alternative in the variant is destoyed and a copy or move of "other" is constructed. If the
		// type is not an unambiguous alternative this overload does not exist.
		//
		template <typename T, typename = variant_helper::enable_convert_assign_t<T, Alternatives...>>
		variant& operator= (T&& other);

		//
		// Observers

		// Returns the index of the alternative contained in the variant
		size_type index() const;

		// Returns true if the variant currently contains the specified alternative type. The queried type
		// must appear in the list of alternatives exactly once.
		template <typename T>
		bool holds_alternative() const;

		// Get a reference to the alternative at the requested index. It is a fatal error if the
		// variant does not contain the requested alternative.
		template <size_type I>
		alternative_t<I>& get() &;

		template <size_type I>
		const alternative_t<I>& get() const&;

		template <size_type I>
		alternative_t<I>&& get() &&;

		template <size_type I>
		const alternative_t<I>&& get() const&&;

		// Get a reference to the alternative of the requested type. Only valid if the variant
		// contains the specified alternative exactly once. It is a fatal error if the
		// variant does not contain the requested alternative.
		template <typename T>
		T& get() &;

		template <typename T>
		const T& get() const&;

		template <typename T>
		T&& get() &&;

		template <typename T>
		const T&& get() const&&;

		// Get a reference to the alternative at the requested index. In release builds there
		// is no check that the variant contains the requested alternative.
		template <size_type I>
		alternative_t<I>& get_unchecked() &;

		template <size_type I>
		const alternative_t<I>& get_unchecked() const&;

		template <size_type I>
		alternative_t<I>&& get_unchecked() &&;

		template <size_type I>
		const alternative_t<I>&& get_unchecked() const&&;

		// Get a reference to the alternative of the requested type. Only valid if the variant
		// contains the specified alternative exactly once. In release builds there
		// is no check that the variant contains the requested alternative.
		template <typename T>
		T& get_unchecked() &;

		template <typename T>
		const T& get_unchecked() const&;

		template <typename T>
		T&& get_unchecked() &&;

		template <typename T>
		const T&& get_unchecked() const&&;

		// Get a pointer to the alternative at the requested index or nullptr if the variant holds
		// a different alternative
		template <size_type I>
		alternative_t<I>* get_if();

		template <size_type I>
		const alternative_t<I>* get_if() const;

		// Get a pointer to the alternative of the requested type if the variant contains that alternative
		// and nullptr if it does not. Only valid if the variant contains the specified alternative exactly once.
		template <typename T>
		T* get_if();

		template <typename T>
		const T* get_if() const;

		// Call the provided function with a reference to the current alternative. The provided function must
		// have an overload for all possible alternatives and the return type is deduced as the result
		// of calling the visitor on the first alternative.
		template <typename V>
		decltype(auto) visit(V&& visitor) &;

		template <typename V>
		decltype(auto) visit(V&& visitor) const&;

		template <typename V>
		decltype(auto) visit(V&& visitor) &&;

		template <typename V>
		decltype(auto) visit(V&& visitor) const&&;

		// Call the provided function with a reference to the current alternative and return a value of the specified
		// type. The provided function must have an overload for all possible alternatives. If the return type is
		// void (even if const or volatile qualified), the return type of the visitor function is ignored.
		template <typename R, typename V>
		R visit(V&& visitor) &;

		template <typename R, typename V>
		R visit(V&& visitor) const&;

		template <typename R, typename V>
		R visit(V&& visitor) &&;

		template <typename R, typename V>
		R visit(V&& visitor) const&&;

		//
		// Modifiers

		// Construct a the requested alternative using the provided arguments. If the alternative is specified by type,
		// it must refer to a type that appears in the list of alternatives exactly once.
		template <typename T, typename... Args>
		T& emplace(Args&&... args);

		template <size_type I, typename... Args>
		alternative_t<I>& emplace(Args&&... args);

		// Exchange the contents of the 2 variants
		void swap(variant& rhs);

		//
		// Comparisons

		// If the variants contain the same alternatives, the result is the result of comparing their respective values.
		// Otherwise, the results is the comparison of the index of the alternative held by each variant.

		bool operator==(const variant& rhs) const;
		bool operator!=(const variant& rhs) const;
		bool operator<(const variant& rhs) const;
		bool operator>(const variant& rhs) const;
		bool operator<=(const variant& rhs) const;
		bool operator>=(const variant& rhs) const;

	private:
		using dispatched_operations = variant_helper::dispatched_operations<Alternatives...>;
		friend class variant_helper::dispatched_operations<Alternatives...>;

		using block_type = typename std::aligned_union<0, Alternatives...>::type;

		static constexpr size_t alternative_count = sizeof...(Alternatives);

		template <typename T>
		static constexpr bool alternative_exists = variant_helper::alternative_exists<T, Alternatives...>::value;

		template <typename T>
		static constexpr bool alternative_exists_once = variant_helper::alternative_exists_once<T, Alternatives...>::value;

		template <typename T>
		static constexpr size_type alternative_type_index = variant_helper::alternative_type_index<T, Alternatives...>::value;

		template <size_type I>
		using alternative_type_at_index = typename variant_helper::alternative_type_at_index<I, Alternatives...>::type;

		//
		// Helpers

		// Dispatch to 2 lvalues with the same constness
		template <typename R, typename V>
		static R dispatch_binary(size_type type_index, block_type& block_1, block_type& block_2, V&& visitor);

		template <typename R, typename V>
		static R dispatch_binary(size_type type_index, const block_type& block_1, const block_type& block_2, V&& visitor);

		// Construct an alternative and set the index
		template <typename T, typename... Args>
		T& construct_contents(size_type index, Args&&... args);

		// Handle copying or moving another variant
		void copy_construct_contents(const variant& other);
		void copy_assign_contents(const variant& other);

		void move_construct_contents(variant& other);
		void move_assign_contents(variant& other);

		// Construct a type by copy or move converting another type
		template <typename T> void convert_construct_contents(typename std::remove_reference<T>::type& lvalue_reference);
		template <typename T> void convert_assign_contents(typename std::remove_reference<T>::type& lvalue_reference);

		template <typename T> void convert_construct_contents(typename std::remove_reference<T>::type&& rvalue_reference);
		template <typename T> void convert_assign_contents(typename std::remove_reference<T>::type&& rvalue_reference);

		// Destroy the current alternative
		void destroy_contents();

		//
		// Data

		block_type m_block;
		size_type m_index;
	};

	// Specialize the get free function. The same as calling the variant::get member function, but may be useful in generic code.
	template <variant_size_type I, typename... As>
	variant_alternative_t<I, variant<As...>>& get(variant<As...>& v);

	template <variant_size_type I, typename... As>
	const variant_alternative_t<I, variant<As...>>& get(const variant<As...>& v);

	template <variant_size_type I, typename... As>
	variant_alternative_t<I, variant<As...>>&& get(variant<As...>&& v);

	template <variant_size_type I, typename... As>
	const variant_alternative_t<I, variant<As...>>&& get(const variant<As...>&& v);

	template <typename T, typename... As>
	T& get(variant<As...>& v);

	template <typename T, typename... As>
	const T& get(const variant<As...>& v);

	template <typename T, typename... As>
	T&& get(variant<As...>&& v);

	template <typename T, typename... As>
	const T&& get(const variant<As...>&& v);

	// Specialize the get_if free function. The same as calling the variant::get_if member function, but may be useful in generic code.
	template <variant_size_type I, typename... As>
	typename std::add_pointer<variant_alternative_t<I, variant<As...>>>::type get_if(variant<As...>& v);

	template <variant_size_type I, typename... As>
	typename std::add_pointer<const variant_alternative_t<I, variant<As...>>>::type get_if(const variant<As...>& v);

	template <typename T, typename... As>
	typename std::add_pointer<T>::type get_if(variant<As...>& v);

	template <typename T, typename... As>
	typename std::add_pointer<const T>::type get_if(const variant<As...>& v);

	// Call the provided function with a reference to the current alternative of each specified variant. The
	// provided function must have an overload for every possible combination of alternatives and the return
	// type is deduced as the result of calling the visitor on the first alternative of each variant.
	template <typename Visitor, typename... Args>
	decltype(auto) visit(Visitor&& visitor, Args&&... args);

	// Call the provided function with a reference to the current alternative of each specified variant and
	// return a value of the specified type. The provided function must have an overload for every possible
	// combination of alternatives. If the return type is void (even if const or volatile qualified), the
	// return type of the visitor function is ignored.
	template <typename R, typename Visitor, typename... Args>
	R visit(Visitor&& visitor, Args&&... args);

	// Swap specialization. Calls the swap member function.
	template <typename... As>
	void swap(variant<As...>& lhs, variant<As...>& rhs);
}

#include "variant.inl"

#endif // !defined(CRN_VARIANT_H)