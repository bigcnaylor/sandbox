#pragma once

#if !defined(CRN_MONOSTATE_H)
#define CRN_MONOSTATE_H

namespace crn {

// Unit type intended for use as a well-behaved empty alternative in variant.
// See std::monostate (https://en.cppreference.com/w/cpp/utility/variant/monostate)
//
struct monostate {
};

// Comparisons
constexpr bool operator== (monostate, monostate) noexcept { return true; }
constexpr bool operator!= (monostate, monostate) noexcept { return false; }
constexpr bool operator<  (monostate, monostate) noexcept { return false; }
constexpr bool operator>  (monostate, monostate) noexcept { return false; }
constexpr bool operator<= (monostate, monostate) noexcept { return true; }
constexpr bool operator>= (monostate, monostate) noexcept { return true; }

}

// Specialised implementation of std::hash
//
template <>
struct std::hash<crn::monostate> {
	std::size_t operator() (const crn::monostate&) const
	{
		// Arbitrary value
		return 0xb16c1337;
	}
};

#endif // !defined(CRN_MONOSTATE_H)